package ln2.foodpicker.util;

import android.util.Log;

import ln2.foodpicker.model.User;

public class CurrentUserSingleton {
    private static final String TAG = "CurrentUserSingle";
    private static User sInstance;

    private CurrentUserSingleton() {
    }

    public static User getCurrentUser() {
        if (sInstance == null) {
            sInstance = new User();
            Log.d("vccc", "hú hú");
        }
        return sInstance;
    }

    public static void setData(String userId,String userName, String password, String email, String imagePath) {
        sInstance = new User(userId, userName, password, email, imagePath);
        Log.d("vccc", "hí hí");
    }
}

