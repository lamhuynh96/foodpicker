package ln2.foodpicker.util;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import ln2.foodpicker.R;

import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class Util {
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static ProgressDialog showDialog(String message, Context context) {
        ProgressDialog dialog = new ProgressDialog(context, R.style.AppTheme_Dark_Dialog);
        dialog.setIndeterminate(true);
        dialog.setMessage(message);
        dialog.show();
        return dialog;
    }

    public static void savePref(Context context, String prefName, String key, String value) {
        SharedPreferences pre = context.getSharedPreferences(prefName, MODE_PRIVATE);
        SharedPreferences.Editor editor = pre.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getPref(Context context, String prefName, String key) {
        SharedPreferences pre = context.getSharedPreferences(prefName, MODE_PRIVATE);
        return pre.getString(key, "");
    }

    public static void showLocationRequestDialog1(final Activity activity, final int requestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Cần quyền truy cập vị trí");
        builder.setMessage("Ứng dụng cần quyền truy cập vị trí.");
        builder.setPositiveButton("Cấp quyền", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        requestCode);
            }
        });
        builder.setNegativeButton("Huỷ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public static void showErrorDialog(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }


    public static boolean isLocationServiceEnabled(Context context){
        LocationManager locationManager = null;
        boolean gps_enabled= false,network_enabled = false;

        if (locationManager == null)
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }
        catch(Exception ex){}

        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }
        catch(Exception ex){}
        return gps_enabled || network_enabled;
    }

    public static void showAddress(LatLng latLng, Context context, TextView textView) {
        Geocoder gcd = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> geoAddresses = gcd.getFromLocation(latLng.latitude, latLng.longitude, 1);
            String address = "";
            if (geoAddresses.size() > 0) {
                for (int i = 0; i < 3; i++) {
                    address += geoAddresses.get(0).getAddressLine(i).replace(",", "") + ", ";
                }
            }

            textView.setText(address.substring(0, address.length() - 2));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static String saveImage(Context context, Uri imageUri) {
        if (imageUri == null)
            return "null";
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), imageUri);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream);
            byte[] byteArray = stream.toByteArray();
            String base64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
            if (base64.equals(""))
                base64 = "null";
            return base64;
        }
        catch (IOException e) {
            return "null";
        }
    }

    public static String saveImage(Context context, Bitmap bitmap) {
        Bitmap thumbnail = Bitmap.createScaledBitmap(bitmap, 250, 200, true);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 60, stream);
        byte[] byteArray = stream.toByteArray();
        String base64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
        if (base64.equals(""))
            base64 = "null";
        return base64;
    }

    public static String createCarURL(Context context, LatLng origin, LatLng dest) {
        return context.getString(R.string.get_direction_url) + "origin=" + String.valueOf(origin.latitude)
                + "," + origin.longitude + "&destination=" + String.valueOf(dest.latitude) + "," +
                String.valueOf(dest.longitude) + "&language=" +"vi&key=" + context.getString(R.string.API_Key);
    }

    public static String createWalkingURL(Context context, LatLng origin, LatLng dest) {
        return context.getString(R.string.get_direction_url) + "origin=" + String.valueOf(origin.latitude)
                + "," + origin.longitude + "&destination=" + String.valueOf(dest.latitude) + "," +
                String.valueOf(dest.longitude) + "&language=" +"vi&mode=walking&key=" + context.getString(R.string.API_Key);
    }

    public static String createBusURL(Context context, LatLng origin, LatLng dest) {
        return context.getString(R.string.get_direction_url) + "origin=" + String.valueOf(origin.latitude)
                + "," + origin.longitude + "&destination=" + String.valueOf(dest.latitude) + "," +
                String.valueOf(dest.longitude) + "&language=" +"vi&mode=transit&key=" + context.getString(R.string.API_Key);
    }
}
