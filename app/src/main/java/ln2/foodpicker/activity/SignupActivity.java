package ln2.foodpicker.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import org.json.JSONException;
import org.json.JSONObject;
import ln2.foodpicker.R;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import ln2.foodpicker.util.Util;
import ln2.foodpicker.util.VolleySingleton;

public class SignupActivity extends AppCompatActivity {

    @BindView(R.id.input_email) EditText edtEmail;
    @BindView(R.id.input_password) EditText edtPassword;
    @BindView(R.id.input_name) EditText edtName;
    @BindView(R.id.btn_signup) Button btnSignup;
    @BindView(R.id.link_login) TextView txvLinkLogin;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        getSupportActionBar().hide();
        ButterKnife.bind(this);

        txvLinkLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivityForResult(intent, 0);
                finish();
            }
        });

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doSignup();
            }
        });
    }

    private void doSignup() {
        progressDialog = Util.showDialog("Đang đăng kí", SignupActivity.this);
        final String name = edtName.getText().toString();
        final String email = edtEmail.getText().toString();
        final String password = edtPassword.getText().toString();

        String URL = getString(R.string.signup_url);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        if (!jsonObject.getBoolean("success")) {
                            Toast.makeText(SignupActivity.this,
                                    "Không thể đăng kí, xin hãy thử lại", Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            Toast.makeText(SignupActivity.this,
                                    "Tạo tài khoản thành công, xin hãy đăng nhập lại!", Toast.LENGTH_SHORT).show();
                            startActivity(i);
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(SignupActivity.this,
                            "Không thể kết nối Server, xin hãy thử lại!", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                params.put("username", name);
                return params;
            }
        };

        VolleySingleton.getInstance(SignupActivity.this).getRequestQueue().add(stringRequest);
    }
}
