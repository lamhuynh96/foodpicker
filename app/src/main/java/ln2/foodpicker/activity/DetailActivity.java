package ln2.foodpicker.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import ln2.foodpicker.R;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ln2.foodpicker.adapter.CommentListAdapter;
import ln2.foodpicker.model.Comment;
import ln2.foodpicker.model.FoodStall;
import ln2.foodpicker.util.CurrentUserSingleton;
import ln2.foodpicker.util.SessionManager;
import ln2.foodpicker.util.Util;
import ln2.foodpicker.util.VolleySingleton;

import static android.graphics.Color.TRANSPARENT;
import static android.view.View.VISIBLE;

public class DetailActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener,
        View.OnClickListener {
    private Gson gson;
    private FoodStall stall;
    private CommentListAdapter commentListAdapter;
    private ArrayList<Comment> comments;
    private SessionManager sessionManager;

    @BindView(R.id.image_stall) ImageView imageStall;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.toolbar_layout) CollapsingToolbarLayout toolbarLayout;
    @BindView(R.id.tv_address) TextView tvAddress;
    @BindView(R.id.tv_name) TextView tvName;
    @BindView(R.id.tv_cost) TextView tvCost;
    @BindView(R.id.tv_time) TextView tvTime;
    @BindView(R.id.tv_description) TextView tvDescription;
    @BindView(R.id.layout_user_comment) LinearLayout layoutUserComment;
    @BindView(R.id.rating) RatingBar ratingBar;
    @BindView(R.id.edt_comment) EditText edtComment;
    @BindView(R.id.list_comment) ExpandableHeightListView listComment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        addEvents();

    }

    private void addEvents() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        gson = new GsonBuilder().create();
        stall = gson.fromJson(getIntent().getStringExtra("detail"), FoodStall.class);
        comments = new ArrayList<>();
        commentListAdapter = new CommentListAdapter(this, comments);
        sessionManager = new SessionManager(this);
        listComment.setAdapter(commentListAdapter);
        listComment.setExpanded(true);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Thông tin chi tiết");
        toolbarLayout.setTitle("");
        toolbarLayout.setExpandedTitleColor(TRANSPARENT);

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.miniMap);
        mapFragment.getMapAsync(this);

        if (stall.getImage_path() != null) {
            Glide.with(this).load(stall.getImage_path()).into(imageStall);
        } else {
            Glide.with(this).load(R.drawable.logo).into(imageStall);
        }

        tvName.setText(stall.getStallName());
        tvAddress.setText(stall.getAddress() + ", " + stall.getDistrict());
        tvTime.setText(stall.getOpenhour() + " - " + stall.getClosehour());
        tvCost.setText(stall.getPrice() + " VND");
        tvDescription.setText(stall.getDescr());

        findViewById(R.id.fab_share).setOnClickListener(this);
        findViewById(R.id.btn_send).setOnClickListener(this);
        getComments(getString(R.string.comment_list_url) + stall.getStallId());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_share:
                Intent shareIntent = new Intent();
                shareIntent.setAction("android.intent.action.SEND");
                shareIntent.putExtra("android.intent.extra.TEXT", "Tên quán: " + stall.getStallName() + " \nĐịa chỉ: " + stall.getAddress() + ", " + stall.getDistrict() +
                        " \nGiá cả: " + stall.getPrice() + " VND" + " \nGiờ mở cửa: " + stall.getOpenhour() + " - " + stall.getClosehour());
                shareIntent.setType("text/plain");
                startActivity(Intent.createChooser(shareIntent, "Chia sẻ"));
                break;
            case R.id.btn_send:
                if (!checkLogin()) {
                    return;
                }

                if (edtComment.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Hãy nhập bình luận.", Toast.LENGTH_SHORT).show();
                    return;
                }

                sendComment(getString(R.string.send_user_comment) + sessionManager.getUserDetails().get("userId")
                                + "&stallId=" + stall.getStallId() + "&comment=" + Uri.encode(edtComment.getText().toString()));

                if (ratingBar.getNumStars() == 0) {
                    sendRating(getString(R.string.send_user_rating) + sessionManager.getUserDetails().get("userId")
                            + "&stallId=" + stall.getStallId() + "&rating=3");
                } else {
                    sendRating(getString(R.string.send_user_rating) + sessionManager.getUserDetails().get("userId")
                            + "&stallId=" + stall.getStallId() + "&rating=" + Math.round(ratingBar.getRating()));
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_favorite) {
            if (!checkLogin()) {
                return false;
            }

            sendFavorite(getString(R.string.send_stall_favorite) + sessionManager.getUserDetails().get("userId") + "&stallId=" + stall.getStallId());
            Log.i("url", getString(R.string.send_stall_favorite) + sessionManager.getUserDetails().get("userId") + "&stallId=" + stall.getStallId());
            return true;
        } else if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean checkLogin() {
        if (!sessionManager.isLoggedIn()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("");
            builder.setMessage("Bạn cần đăng nhập để lưu địa điểm này.");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent login = new Intent(DetailActivity.this, LoginActivity.class);
                    startActivity(login);
                }
            });
            builder.setNegativeButton("Huỷ", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            dialog.cancel();
                        }
            });
            builder.show();
            return false;
        }
        return true;
    }

    private void sendComment(String URL) {
        StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getBoolean("success")) {
                            Toast.makeText(DetailActivity.this, "Đã lưu bình luận.", Toast.LENGTH_SHORT).show();
                            getComments(getString(R.string.comment_list_url) + stall.getStallId());
                        } else {
                            Toast.makeText(DetailActivity.this, "Lưu bình luận thất bại. Hãy thử lại.", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(DetailActivity.this, "Lưu bình luận thất bại. Hãy thử lại.", Toast.LENGTH_SHORT).show();
                    }
                });
        VolleySingleton.getInstance(this).getRequestQueue().add(strReq);
    }

    private void sendRating(String URL) {
        StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getBoolean("success")) {
                            Toast.makeText(DetailActivity.this, "Đã đánh giá.", Toast.LENGTH_SHORT).show();
                            ratingBar.setNumStars(0);
                            edtComment.setText("");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(DetailActivity.this, "Lưu đánh giá thất bại. Hãy thử lại.", Toast.LENGTH_SHORT).show();
                    }
                });
        VolleySingleton.getInstance(this).getRequestQueue().add(strReq);
    }

    private void sendFavorite(String URL) {
        StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        if (!jsonObject.getBoolean("success")) {
                            Toast.makeText(DetailActivity.this, "Bạn đã lưu địa điểm này trước đó.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(DetailActivity.this, "Lưu thành công.", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(DetailActivity.this, "Lưu địa điểm thất bại. Hãy thử lại.", Toast.LENGTH_SHORT).show();
                    }
                });
        VolleySingleton.getInstance(this).getRequestQueue().add(strReq);
    }

    private void getComments(String URL) {
        StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("payload");
                        List result = Arrays.asList(gson.fromJson(jsonArray.toString(), Comment[].class));

                        if (!result.isEmpty()) {
                            comments.clear();
                            comments.addAll(result);
                            commentListAdapter.notifyDataSetChanged();
                            listComment.clearFocus();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(DetailActivity.this, "Lưu địa điểm thất bại. Hãy thử lại.", Toast.LENGTH_SHORT).show();
                    }
                });
        VolleySingleton.getInstance(this).getRequestQueue().add(strReq);
    }

    public void onMapReady(GoogleMap map) {
        map.getUiSettings().setZoomControlsEnabled(false);
        map.getUiSettings().setCompassEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.getUiSettings().setAllGesturesEnabled(true);
        map.addMarker(new MarkerOptions().position(new LatLng(stall.getLatitude(), stall.getLongitude())));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(stall.getLatitude(), stall.getLongitude()), 18.0F));
        map.setOnMapClickListener(this);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Intent fullMap = new Intent(this, FullMapActivity.class);
        fullMap.putExtra("latitude", stall.getLatitude());
        fullMap.putExtra("longitude", stall.getLongitude());
        fullMap.putExtra("address", stall.getAddress() + ", " + stall.getDistrict());
        startActivity(fullMap);
    }
}
