package ln2.foodpicker.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import ln2.foodpicker.R;

import android.support.v7.view.ActionMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ln2.foodpicker.adapter.FavoriteListAdapter;
import ln2.foodpicker.model.FoodStall;
import ln2.foodpicker.util.SessionManager;
import ln2.foodpicker.util.VolleySingleton;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class FavoriteActivity extends AppCompatActivity
        implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, AdapterView.OnItemLongClickListener,
        AdapterView.OnItemClickListener {

    private FavoriteListAdapter adapter;
    private ArrayList<FoodStall> stalls;
    private ArrayList<FoodStall> temps;
    private Integer stallNumber = 0;
    private List<FoodStall> selectedStalls;
    private ActionModeCallback actionModeCallback;
    private ActionMode actionMode = null;

    @BindView(R.id.list_food) ListView listFood;
    @BindView(R.id.layout_swipe) SwipeRefreshLayout layoutSwipe;
    @BindView(R.id.layout_try_again) LinearLayout layoutTryAgain;
    @BindView(R.id.layout_empty) LinearLayout layoutEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Địa điểm yêu thích");
        stalls = new ArrayList<>();
        temps = new ArrayList<>();
        selectedStalls = new ArrayList<>();
        adapter = new FavoriteListAdapter(this, stalls);
        listFood.setAdapter(adapter);
        listFood.setOnItemLongClickListener(this);
        listFood.setOnItemClickListener(this);
        actionModeCallback = new ActionModeCallback();
        layoutSwipe.setOnRefreshListener(this);
        findViewById(R.id.btn_try_again).setOnClickListener(this);
        getFavoriteList(getString(R.string.get_favorite_list) +
                new SessionManager(this).getUserDetails().get(SessionManager.KEY_ID));
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_try_again) {
            getFavoriteList(getString(R.string.get_favorite_list) +
                    new SessionManager(this).getUserDetails().get(SessionManager.KEY_ID));
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (actionMode == null) {
            Intent detail = new Intent(this, DetailActivity.class);
            detail.putExtra("detail", new Gson().toJson(adapter.getItem(i)));
            startActivity(detail);
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (actionMode == null) {
            actionMode = startSupportActionMode(actionModeCallback);
        }

        if (selectedStalls.contains(adapter.getItem(i))) {
            listFood.setItemChecked(i, false);
            stallNumber--;
            actionMode.setTitle(String.valueOf(stallNumber));
            actionMode.invalidate();
            selectedStalls.remove(adapter.getItem(i));
            return false;
        }

        stallNumber++;
        selectedStalls.add(adapter.getItem(i));


        if (stallNumber > 2) {
            Toast.makeText(this, "Không thể chọn hơn 2 địa điểm.", Toast.LENGTH_SHORT).show();
            listFood.setItemChecked(i, false);
            stallNumber = 2;
            return false;
        }

        actionMode.setTitle(String.valueOf(stallNumber));
        actionMode.invalidate();
        return false;
    }

    @Override
    public void onRefresh() {
        getFavoriteList(getString(R.string.get_favorite_list) +
                new SessionManager(this).getUserDetails().get(SessionManager.KEY_ID));
    }

    private void getFavoriteList(String URL) {
        layoutSwipe.setRefreshing(true);

        StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                layoutSwipe.setRefreshing(false);
                if (response != null) {
                    listFood.setVisibility(VISIBLE);
                    layoutTryAgain.setVisibility(GONE);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("payload");
                        List result = Arrays.asList(new Gson().fromJson(jsonArray.toString(), FoodStall[].class));

                        if (result.isEmpty()) {
                            listFood.setVisibility(GONE);
                            layoutTryAgain.setVisibility(GONE);
                            layoutEmpty.setVisibility(VISIBLE);
                            return;
                        }

                        stalls.clear();
                        stalls.addAll(result);
                        temps.addAll(result);
                        adapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        layoutSwipe.setRefreshing(false);
                        listFood.setVisibility(GONE);
                        layoutTryAgain.setVisibility(VISIBLE);
                    }
                });
        VolleySingleton.getInstance(this).getRequestQueue().add(strReq);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_action_mode, menu);
            layoutSwipe.setEnabled(false);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_direct:
                    Intent fullMap = new Intent(FavoriteActivity.this, FullMapActivity.class);

                    if (selectedStalls.size() == 2) {
                        fullMap.putExtra("origin", new Gson().toJson(selectedStalls.get(0)));
                        fullMap.putExtra("destination", new Gson().toJson(selectedStalls.get(1)));
                    } else {
                        fullMap.putExtra("destination", new Gson().toJson(selectedStalls.get(0)));
                    }
                    startActivity(fullMap);
                    stalls.clear();
                    stalls.addAll(temps);
                    adapter.notifyDataSetChanged();
                    actionMode = null;
                    mode.finish();
                    return true;
                case android.R.id.home:
                    actionMode = null;
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            adapter.notifyDataSetChanged();
            layoutSwipe.setEnabled(true);
            selectedStalls.clear();
            stalls.clear();
            stalls.addAll(temps);
            adapter.notifyDataSetChanged();
            actionMode = null;
            stallNumber = 0;
        }
    }
}
