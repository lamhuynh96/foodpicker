package ln2.foodpicker.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import ln2.foodpicker.R;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import ln2.foodpicker.adapter.CircleTransform;
import ln2.foodpicker.adapter.CustomInfowindowAdapter;
import ln2.foodpicker.adapter.CustomSearchAdapter;
import ln2.foodpicker.model.FoodStall;
import ln2.foodpicker.util.CurrentUserSingleton;
import ln2.foodpicker.util.NotificationUtil;
import ln2.foodpicker.util.SessionManager;
import ln2.foodpicker.util.Util;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lapism.searchview.SearchItem;
import com.lapism.searchview.SearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ln2.foodpicker.util.VolleySingleton;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.bumptech.glide.load.engine.DiskCacheStrategy.NONE;

public class SearchAroundActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener,
        GoogleMap.OnInfoWindowClickListener, SearchView.OnMenuClickListener, SearchView.OnQueryTextListener,
        CustomSearchAdapter.OnItemClickListener {
    private static final int ACCESS_FINE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_FINE_PERMISSION_SETTING = 102;

    private enum MapView {
        NotifyAreaView,
        SearchAround
    }

    MapView mapView = MapView.SearchAround;

    private ActionBarDrawerToggle toggle;
    private SharedPreferences locationPermissionStatus;
    private HashMap<String, FoodStall> markerInfo;
    private ArrayList<Marker> markers;
    private ArrayList<FoodStall> stalls;
    private LatLngBounds.Builder builder;
    private LocationRequest mLocationRequest;
    private Location currentLocation;
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mMap;
    private Gson gson;
    private boolean hasData;
    private boolean choosingArea = false;
    private String radius = "500";
    boolean cancel = false;
    private double radiusArea = 0.5;

    private TextView txvName, txvEmail;
    private ImageView imgProfile;
    private SessionManager session;
    private HashMap<String, String> userInfor;
    private View navHeader;
    private CustomSearchAdapter searchAdapter;
    private List<SearchItem> keywordList;
    private boolean hadArea = false;

    private Location locationArea;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.layout_search_around) RelativeLayout layoutSearchAround;
    @BindView(R.id.tv_current_address) TextView tvAddress;
    @BindView(R.id.tv_radius) TextView tvRadius;
    @BindView(R.id.search_view) SearchView searchView;
    @BindView(R.id.notification) FrameLayout notification;
    @BindView(R.id.tv_noti_number) TextView tvNotificationNumber;
    @BindView(R.id.btn_Switch) ToggleButton btn_switch;
    @BindView(R.id.txv_Choice) TextView tv_choice;
    @BindView(R.id.btn_edit) Button btn_edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_around);
        session = new SessionManager(getApplicationContext());
        ButterKnife.bind(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        addEvents();
        buildGoogleApiClient();
    }

    private void addEvents() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        btn_edit.setVisibility(View.GONE);
        navHeader = navigationView.getHeaderView(0);
        txvName = (TextView) navHeader.findViewById(R.id.tv_userName);
        txvEmail = (TextView) navHeader.findViewById(R.id.tv_email);
        imgProfile = (ImageView) navHeader.findViewById(R.id.img_profile);
        tvNotificationNumber.setVisibility(GONE);
        userInfor = session.getUserDetails();
        CurrentUserSingleton.setData(userInfor.get(SessionManager.KEY_ID),
                userInfor.get(SessionManager.KEY_NAME),
                userInfor.get(SessionManager.KEY_PASSWORD),
                userInfor.get(SessionManager.KEY_EMAIL),
                userInfor.get(SessionManager.KEY_IMAGE));

        txvName.setText(userInfor.get(SessionManager.KEY_NAME));
        txvEmail.setText(userInfor.get(SessionManager.KEY_EMAIL));

        if (userInfor.get(SessionManager.KEY_IMAGE).equals("")) {
            Glide.with(this).load(R.drawable.logo)
                    .crossFade()
                    .thumbnail(0.5f)
                    .diskCacheStrategy(NONE)
                    .skipMemoryCache(true)
                    .bitmapTransform(new CircleTransform(this))
                    .into(imgProfile);

        } else {
            Glide.with(this).load(userInfor.get(SessionManager.KEY_IMAGE))
                    .crossFade()
                    .thumbnail(0.5f)
                    .diskCacheStrategy(NONE)
                    .skipMemoryCache(true)
                    .bitmapTransform(new CircleTransform(this))
                    .into(imgProfile);

        }


        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        toggle = new ActionBarDrawerToggle(this, drawer, toolbar,  R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (searchView != null && searchView.isSearchOpen()) {
                    searchView.close(true);
                }
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        btn_switch.setOnClickListener(this);
        btn_edit.setOnClickListener(this);
        toggle.syncState();
        drawer.addDrawerListener(toggle);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        markerInfo = new HashMap<>();
        markers = new ArrayList<>();
        gson = new GsonBuilder().create();
        hasData = false;
        tvRadius.setOnClickListener(this);
        notification.setOnClickListener(this);
        getKeywordList();
        setUpSearchView();


    }

    private void setUpSearchView() {
        searchView.setHint("Nhập từ khoá ...");
        searchView.setOnQueryTextListener(this);
        searchView.setVoice(false);

        keywordList = new ArrayList<>();
        searchAdapter = new CustomSearchAdapter(this, keywordList);
        searchAdapter.addOnItemClickListener(this);
        searchView.setAdapter(searchAdapter);
        searchView.setOnMenuClickListener(this);
        searchView.setShadow(false);
    }

    @Override
    public void onMenuClick() {
        drawer.openDrawer(GravityCompat.START);
    }

    @Override
    public void onItemClick(View view, int i) {
        mapView = MapView.SearchAround;
        btn_switch.setChecked(false);
        searchView.close(false);
        tv_choice.setText("Tìm xung quanh");
        TextView textView = (TextView) view.findViewById(R.id.textView_item_text);
        searchView.setTextOnly(textView.getText().toString());

        if (currentLocation != null) {
            getFoodStall(getString(R.string.get_stall_by_distance) + "longitude=" + currentLocation.getLongitude()
                    + "&latitude=" + currentLocation.getLatitude() + "&radius=" + radius + "&keyword=" + Uri.encode(textView.getText().toString()));
            hasData = false;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mapView = MapView.SearchAround;
        btn_switch.setChecked(false);
        searchView.close(false);
        if (query.equals(""))
        {
            getFoodStall(getString(R.string.get_stall_by_distance) + "longitude=" + currentLocation.getLongitude()
                    + "&latitude=" + currentLocation.getLatitude() + "&radius=4000");
            return true;
        }

        if (notification.getVisibility() == GONE) {
            notification.setVisibility(VISIBLE);
        }

        if (currentLocation != null) {
            getFoodStall(getString(R.string.get_stall_by_distance) + "longitude=" + currentLocation.getLongitude()
                    + "&latitude=" + currentLocation.getLatitude() + "&radius=" + radius + "&keyword=" + Uri.encode(query));
            hasData = false;
        }

        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText.equals("")) {

            if (notification.getVisibility() == GONE) {
                notification.setVisibility(VISIBLE);
            }

            if (!hasData) {
                getFoodStall(getString(R.string.get_stall_by_distance) + "longitude=" + currentLocation.getLongitude()
                        + "&latitude=" + currentLocation.getLatitude() + "&radius=4000");
                hasData = true;
                return true;
            }
        }

        if (!newText.equals("") && notification.getVisibility() == VISIBLE) {
            notification.setVisibility(GONE);
        }
        return true;
    }

    public void getKeywordList() {
        StringRequest strReq = new StringRequest(Request.Method.GET, getString(R.string.get_keyword_list), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject res = new JSONObject(response);
                        String keywords = res.getString("payload");
                        String []tokens = keywords.split(", ");

                        for (String token : tokens) {
                            keywordList.add(new SearchItem(token));
                        }

                        searchAdapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Snackbar snackbar = Snackbar
                                .make(layoutSearchAround, "Tải danh sách từ khoá thất bại", Snackbar.LENGTH_LONG)
                                .setAction("Thử lại", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        getKeywordList();
                                    }
                                });

                        snackbar.show();
                    }
                });
        VolleySingleton.getInstance(this).getRequestQueue().add(strReq);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(SearchAroundActivity.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    @Override
    public void onMapReady(final GoogleMap map) {
        map.getUiSettings().setZoomControlsEnabled(false);
        map.getUiSettings().setCompassEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.getUiSettings().setAllGesturesEnabled(true);
        map.setInfoWindowAdapter(new CustomInfowindowAdapter(this, markerInfo));
        map.setOnInfoWindowClickListener(this);
        mMap = map;

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (choosingArea) {
                    Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(latLng.latitude, latLng.longitude)));
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker1));
                    markers.add(marker);
                    locationArea = new Location("");
                    locationArea.setLatitude(latLng.latitude);
                    locationArea.setLongitude(latLng.longitude);

                    AlertDialog.Builder builder = new AlertDialog.Builder(SearchAroundActivity.this);
                    builder.setTitle("Chọn bán kính vùng có tâm đã chọn");
                    String[] radius = {"500 m", "1 km", "1.5 km", "2 km", "3 km"};
                    builder.setItems(radius, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    radiusArea = 0.5;
                                    break;
                                case 1:
                                    radiusArea = 1;
                                    break;
                                case 2:
                                    radiusArea = 1.5;
                                    break;
                                case 3:
                                    radiusArea = 2;
                                    break;
                                case 4:
                                    radiusArea = 3;
                                    break;
                            }
                            dialog.dismiss();
                            String URL = getString(R.string.user_add_area) + "userId=" +
                                    CurrentUserSingleton.getCurrentUser().getUserId() + "&latitude=" +
                                    String.valueOf(locationArea.getLatitude()) + "&longitude=" +
                                    String.valueOf(locationArea.getLongitude()) + "&radius=" +
                                    String.valueOf(radiusArea);

                            Log.d("nghia", URL);

                            StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    if (response != null) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(response);

                                            if (jsonObject.getBoolean("success")) {
                                                Toast.makeText(SearchAroundActivity.this,
                                                        "Cập nhật khoanh vùng thành công",
                                                        Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(SearchAroundActivity.this,
                                                        "Cập nhật khoanh vùng thất bại, xin hãy thử lại",
                                                        Toast.LENGTH_SHORT).show();
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Toast.makeText(SearchAroundActivity.this,
                                                    "Cập nhật khoang vùng không thành công! Xin hãy thử lại!",
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                    });
                            VolleySingleton.getInstance(SearchAroundActivity.this).getRequestQueue().add(strReq);
                            choosingArea = false;
                            mMap.clear();
                            getFoodStall(getString(R.string.get_stall_by_distance) + "longitude=" + currentLocation.getLongitude()
                                    + "&latitude=" + currentLocation.getLatitude() + "&radius=4000");
                            getNotifyArea();
                        }
                    });

                    builder.show();
                }
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            locationPermissionStatus = getSharedPreferences("LOCATION_PERMISSION", MODE_PRIVATE);
            checkLocationPermission();
        } else {
            checkLocationServicedEnabled();
        }

        if (currentLocation != null) {
            getFoodStall(getString(R.string.get_stall_by_distance) + "longitude=" + currentLocation.getLongitude()
                    + "&latitude=" + currentLocation.getLatitude() + "&radius=4000");
        }
    }

    @Override
    public void onInfoWindowClick(Marker paramMarker) {
        FoodStall stall = markerInfo.get(paramMarker.getId());
        Intent detail = new Intent(this, DetailActivity.class);
        detail.putExtra("detail", gson.toJson(stall));
        startActivity(detail);
    }

    @Override
    public void onConnected(Bundle bundle) {
        requestLocation();

        if (ContextCompat.checkSelfPermission(SearchAroundActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int paramInt) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        buildGoogleApiClient();
        Toast.makeText(this, "Lấy vị trí thất bại. Hãy thử lại.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            currentLocation = location;

            if (mapView == MapView.SearchAround) {
                if (!hasData) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 16.0F));
                    getFoodStall(getString(R.string.get_stall_by_distance) + "longitude=" + currentLocation.getLongitude()
                            + "&latitude=" + currentLocation.getLatitude() + "&radius=3000");

                }
                Util.showAddress(new LatLng(location.getLatitude(), location.getLongitude()), this, tvAddress);
                removeLocationUpdates();
            }
            else
            {
                Util.showAddress(new LatLng(location.getLatitude(), location.getLongitude()), this, tvAddress);
                removeLocationUpdates();
            }
        }
    }

    private void requestLocation() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(5000L);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setFastestInterval(1000);
    }

    private void removeLocationUpdates() {
        if (mGoogleApiClient != null) {

            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            }
            mGoogleApiClient.disconnect();
        }
    }

    private void checkLocationPermission() {
        if (ActivityCompat.checkSelfPermission(SearchAroundActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(SearchAroundActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                Util.showLocationRequestDialog1(SearchAroundActivity.this, ACCESS_FINE_PERMISSION_CONSTANT);
            } else if (locationPermissionStatus.getBoolean(Manifest.permission.ACCESS_FINE_LOCATION, false)) {
                showLocationRequestDialog();
            } else {
                ActivityCompat.requestPermissions(SearchAroundActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        ACCESS_FINE_PERMISSION_CONSTANT);
            }
            SharedPreferences.Editor editor = locationPermissionStatus.edit();
            editor.putBoolean(Manifest.permission.ACCESS_FINE_LOCATION, true);
            editor.commit();
        } else {
            checkLocationServicedEnabled();
        }
    }

    private void checkLocationServicedEnabled() {
        if (!Util.isLocationServiceEnabled(getApplicationContext())) {
            showLocationRequestDialog();
        }
        else {
            if (ActivityCompat.checkSelfPermission(SearchAroundActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                requestLocation();
                mMap.setMyLocationEnabled(true);
            }
        }
    }

    private void showLocationRequestDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SearchAroundActivity.this);
        builder.setTitle("Cần quyền truy cập vị trí");
        builder.setMessage("Ứng dụng cần quyền truy cập vị trí.");
        builder.setPositiveButton("Cấp quyền", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(myIntent, REQUEST_FINE_PERMISSION_SETTING);
            }
        });
        builder.setNegativeButton("Huỷ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_FINE_PERMISSION_SETTING) {

            if (ActivityCompat.checkSelfPermission(SearchAroundActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                requestLocation();
                mMap.setMyLocationEnabled(true);
            } else {
                Util.showLocationRequestDialog1(SearchAroundActivity.this, ACCESS_FINE_PERMISSION_CONSTANT);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ACCESS_FINE_PERMISSION_CONSTANT) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if (ContextCompat.checkSelfPermission(SearchAroundActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    checkLocationServicedEnabled();
                }

            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(SearchAroundActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Util.showLocationRequestDialog1(SearchAroundActivity.this, ACCESS_FINE_PERMISSION_CONSTANT);
                } else {
                    Toast.makeText(getBaseContext(), "Không thể lấy quyền truy cập vị trí!", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_radius:
                openPopup();
                break;
            case R.id.notification:
                Intent notification = new Intent(this, NotificationActivity.class);
                startActivity(notification);
                break;
            case R.id.btn_Switch:
                if (mapView == MapView.SearchAround){
                    mapView = MapView.NotifyAreaView;
                    btn_edit.setVisibility(View.VISIBLE);
                    tv_choice.setText("Khoanh vùng thông báo");
                    hasData = false;
                    getNotifyArea();
                } else {
                    btn_edit.setVisibility(View.GONE);
                    mapView = MapView.SearchAround;
                    tv_choice.setText("Tìm quanh đây");
                    if (!hasData) {
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 16.0F));
                        getFoodStall(getString(R.string.get_stall_by_distance) + "longitude=" + currentLocation.getLongitude()
                                + "&latitude=" + currentLocation.getLatitude() + "&radius=3000");

                    }
                    Util.showAddress(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), SearchAroundActivity.this, tvAddress);
                    removeLocationUpdates();
                }
                break;
            case R.id.btn_edit:
                mMap.clear();
                getFoodStall(getString(R.string.get_stall_by_distance) + "longitude=" + currentLocation.getLongitude()
                        + "&latitude=" + currentLocation.getLatitude() + "&radius=4000");
                chooseNotifyArea();
                break;
        }
    }

    private void chooseNotifyArea() {
        choosingArea = true;
        AlertDialog alertDialog = new AlertDialog.Builder(SearchAroundActivity.this).create();
        if (!hadArea) {
            alertDialog.setTitle("Bạn vẫn chưa có vùng thông báo");
        } else {
            alertDialog.setTitle("Chỉnh sửa vùng thông báo");
        }
        alertDialog.setMessage("Xin hãy chọn một địa điểm bất kỳ làm tâm vùng trên bảng đồ");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();

        mMap.clear();

    }

    private void openPopup() {
        PopupMenu popupMenu = new PopupMenu(this, tvRadius);
        popupMenu.getMenuInflater().inflate(R.menu.menu_radius, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.radius_250:
                        tvRadius.setText("250m");
                        radius = "250";
                        getFoodStall(getString(R.string.get_stall_by_distance) + "longitude=" + currentLocation.getLongitude()
                                + "&latitude=" + currentLocation.getLatitude() + "&radius=250");
                        return true;
                    case R.id.radius_500:
                        tvRadius.setText("500m");
                        radius = "500";
                        getFoodStall(getString(R.string.get_stall_by_distance) + "longitude=" + currentLocation.getLongitude()
                                + "&latitude=" + currentLocation.getLatitude() + "&radius=500");
                        return true;
                    case R.id.radius_1000:
                        tvRadius.setText("1km");
                        radius = "1000";
                        getFoodStall(getString(R.string.get_stall_by_distance) + "longitude=" + currentLocation.getLongitude()
                                + "&latitude=" + currentLocation.getLatitude() + "&radius=1000");
                        return true;
                    case R.id.radius_2000:
                        tvRadius.setText("2km");
                        radius = "2000";
                        getFoodStall(getString(R.string.get_stall_by_distance) + "longitude=" + currentLocation.getLongitude()
                                + "&latitude=" + currentLocation.getLatitude() + "&radius=2000");
                        return true;
                    case R.id.radius_3000:
                        tvRadius.setText("3km");
                        radius = "3000";
                        getFoodStall(getString(R.string.get_stall_by_distance) + "longitude=" + currentLocation.getLongitude()
                                + "&latitude=" + currentLocation.getLatitude() + "&radius=3000");
                        return true;
                    case R.id.radius_4000:
                        tvRadius.setText("4km");
                        radius = "4000";
                        getFoodStall(getString(R.string.get_stall_by_distance) + "longitude=" + currentLocation.getLongitude()
                                + "&latitude=" + currentLocation.getLatitude() + "&radius=4000");
                        return true;
                    case R.id.radius_5000:
                        tvRadius.setText("5km");
                        radius = "5000";
                        getFoodStall(getString(R.string.get_stall_by_distance) + "longitude=" + currentLocation.getLongitude()
                                + "&latitude=" + currentLocation.getLatitude() + "&radius=5000");
                        return true;
                    default:
                        return false;
                }
            }
        });
        popupMenu.show();
    }

    private void getNotifyArea()
    {
        String URL = getString(R.string.get_notify_area) + "userId="
                + CurrentUserSingleton.getCurrentUser().getUserId();

        if (mapView == MapView.NotifyAreaView) {
            StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response != null) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArr = jsonObject.getJSONArray("payload");

                            if (jsonArr != null && jsonArr.length() > 0)
                            {
                                hadArea = true;
                                JSONObject userJson = jsonArr.getJSONObject(0);
                                double latitude = userJson.getDouble("latitude");
                                double longitude = userJson.getDouble("longitude");
                                double radius = userJson.getDouble("radius");

                                builder = new LatLngBounds.Builder();
                                builder.include(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
                                LatLng currentLatLon = new LatLng(latitude,longitude);

                                Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)));
                                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker1));
                                markers.add(marker);
                                builder.include(new LatLng(latitude, longitude));
                                mMap.addCircle(new CircleOptions()
                                        .center(currentLatLon)
                                        .radius(radius * 1000)
                                        .strokeColor(Color.RED)
                                        .fillColor(0x220000FF)
                                        .strokeWidth(5)
                                );

                                LatLng latLng = new LatLng(latitude, longitude);

                                //mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 250), 1000, null);
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, (int) (16 - Math.log(radius * 2) / Math.log(2))));
                            } else {
                                hadArea = true;
                                chooseNotifyArea();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(SearchAroundActivity.this, "Lấy danh sách địa điểm ăn uống thất bại. Hãy thử lại.", Toast.LENGTH_SHORT).show();
                        }
                    });
            VolleySingleton.getInstance(this).getRequestQueue().add(strReq);
        } else {

        }
    }

    private void getFoodStall(String URL) {
        mMap.clear();
        StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("payload");
                        List result = Arrays.asList(gson.fromJson(jsonArray.toString(), FoodStall[].class));

                        if (result.isEmpty()) {
                            Toast.makeText(SearchAroundActivity.this, "Không có địa điểm ăn uống nào trong bán kính đã chọn.", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        stalls = new ArrayList<>();
                        stalls.addAll(result);
                        builder = new LatLngBounds.Builder();
                        builder.include(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));

                        for (FoodStall stall : stalls) {
                            Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(stall.getLatitude(), stall.getLongitude())));
                            markers.add(marker);
                            markerInfo.put(marker.getId(), stall);
                            builder.include(new LatLng(stall.getLatitude(), stall.getLongitude()));
                        }

                        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 200), 1000, null);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SearchAroundActivity.this, "Lấy danh sách địa điểm ăn uống thất bại. Hãy thử lại.", Toast.LENGTH_SHORT).show();
                    }
                });
        VolleySingleton.getInstance(this).getRequestQueue().add(strReq);
    }

    public void getUnseenNotificationNumber(final String URL) {
        StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        JSONObject res = new JSONObject(response);

                        if (res.getInt("payload") > 0) {
                            tvNotificationNumber.setVisibility(VISIBLE);
                            tvNotificationNumber.setText(String.valueOf(res.getInt("payload")));
                        } else {
                            tvNotificationNumber.setVisibility(GONE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        getUnseenNotificationNumber(URL);
                    }
                });
        VolleySingleton.getInstance(this).getRequestQueue().add(strReq);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_around, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_notifications) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_places:
                Intent food = new Intent(this, FoodActivity.class);
                startActivity(food);
                finish();
                break;
            case R.id.nav_favorite:
                Intent favorite = new Intent(this, FavoriteActivity.class);
                startActivity(favorite);
                finish();
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getUnseenNotificationNumber(getString(R.string.get_unseen_noti_number) +
                new SessionManager(this).getUserDetails().get(SessionManager.KEY_ID));
    }

    @Override
    public void onPause() {
        super.onPause();
        removeLocationUpdates();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
    }
}
