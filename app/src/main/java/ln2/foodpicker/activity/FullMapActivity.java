package ln2.foodpicker.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import ln2.foodpicker.R;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ln2.foodpicker.adapter.InstructionListAdapter;
import ln2.foodpicker.asynctask.GetDirection;
import ln2.foodpicker.model.Directions;
import ln2.foodpicker.model.Directions.Leg.Step;
import ln2.foodpicker.model.FoodStall;
import ln2.foodpicker.util.Util;
import ln2.foodpicker.util.VolleySingleton;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class FullMapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, GoogleMap.OnPolylineClickListener,
        View.OnClickListener, ListView.OnTouchListener, AdapterView.OnItemSelectedListener {
    private static final int ACCESS_FINE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_FINE_PERMISSION_SETTING = 102;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 103;

    private SharedPreferences locationPermissionStatus;
    private LocationRequest mLocationRequest;
    private LatLng currentLocation;
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mMap;
    private Double latitude, longitude;
    private boolean firstRun = true;
    private int tabNumber = -1;
    private GetDirection getDirection;
    private ArrayList<Step> steps;
    private InstructionListAdapter adapter;
    private BottomSheetBehavior behavior;
    private LatLngBounds.Builder builder;
    private FoodStall origin, destination;
    private boolean fromFavorite = false;

    @BindView(R.id.tv_origin) TextView tvOrigin;
    @BindView(R.id.app_bar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tv_destination) TextView tvDestination;
    @BindView(R.id.tab_car) LinearLayout tabCar;
    @BindView(R.id.tab_bus) LinearLayout tabBus;
    @BindView(R.id.tab_walking) LinearLayout tabWalking;
    @BindView(R.id.icon_car) ImageView iconCar;
    @BindView(R.id.tv_car) TextView tvCar;
    @BindView(R.id.icon_bus) ImageView iconBus;
    @BindView(R.id.tv_bus) TextView tvBus;
    @BindView(R.id.icon_walking) ImageView iconWalking;
    @BindView(R.id.tv_walking) TextView tvWalking;
    @BindView(R.id.bottom_sheet) View bottomSheet;
    @BindView(R.id.instruction_list) ListView instructionList;
    @BindView(R.id.tv_distance_time) TextView tvDistanceTime;
    @BindView(R.id.fab_direct) FloatingActionButton fabDirection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_map);
        ButterKnife.bind(this);
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        addEvents();
        buildGoogleApiClient();
    }

    private void addEvents() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        iconCar.setImageResource(R.drawable.ic_car);
        tvCar.setTextColor(Color.parseColor("#3F51B5"));
        tabCar.setBackgroundResource(R.drawable.transit_bg_selected);
        tabCar.setOnClickListener(this);
        tabBus.setOnClickListener(this);
        tabWalking.setOnClickListener(this);
        findViewById(R.id.fab_direct).setOnClickListener(this);
        tvOrigin.setOnClickListener(this);

        if (getIntent().getStringExtra("origin") != null) {
            fromFavorite = true;
            origin = new Gson().fromJson(getIntent().getStringExtra("origin"), FoodStall.class);
            tvOrigin.setText(origin.getAddress() + ", " + origin.getDistrict());
            currentLocation = new LatLng(origin.getLatitude(), origin.getLongitude());
        }

        if (getIntent().getStringExtra("destination") != null) {
            destination = new Gson().fromJson(getIntent().getStringExtra("destination"), FoodStall.class);
            tvDestination.setText(destination.getAddress() + ", " + destination.getDistrict());
            latitude = destination.getLatitude();
            longitude = destination.getLongitude();
        }

        if (getIntent().getDoubleExtra("latitude", 0) != 0) {
            latitude = getIntent().getDoubleExtra("latitude", 0);
            longitude = getIntent().getDoubleExtra("longitude", 0);
            tvDestination.setText(getIntent().getStringExtra("address"));
        }

        steps = new ArrayList<>();
        adapter = new InstructionListAdapter(this, steps);
        instructionList.setAdapter(adapter);
        instructionList.setOnTouchListener(this);
        instructionList.setOnItemSelectedListener(this);
        behavior = BottomSheetBehavior.from(bottomSheet);
        tvDistanceTime.setOnTouchListener(this);

        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    toolbar.setVisibility(GONE);
                    appBarLayout.setVisibility(GONE);
                }

                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    appBarLayout.setVisibility(VISIBLE);
                    toolbar.setVisibility(VISIBLE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {}
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        adapter.getItem(i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v.getId() == R.id.instruction_list) {
            v.getParent().requestDisallowInterceptTouchEvent(true);
        } else {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.tv_origin:
                AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                        .setCountry("VN")
                        .build();

                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(typeFilter)
                            .build(this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.tab_car:
                if (tabNumber != 0) {
                    iconCar.setImageResource(R.drawable.ic_car);
                    tvCar.setTextColor(Color.parseColor("#3F51B5"));
                    tabCar.setBackgroundResource(R.drawable.transit_bg_selected);
                    iconBus.setImageResource(R.drawable.ic_bus_white);
                    tvBus.setTextColor(Color.WHITE);
                    tabBus.setBackgroundResource(R.drawable.transit_bg_normal);
                    tabWalking.setBackgroundResource(R.drawable.transit_bg_normal);
                    iconWalking.setImageResource(R.drawable.ic_walking_white);
                    tvWalking.setTextColor(Color.WHITE);
                    fabDirection.setVisibility(VISIBLE);

                    if (currentLocation != null) {
                        mMap.clear();
                        mMap.addMarker(new MarkerOptions().position(currentLocation));
                        getDirection.getCarDirection(Util.createCarURL(this, currentLocation, new LatLng(latitude, longitude)), tvCar);
                    } else {
                        Toast.makeText(this, "Không lấy được vị trí. Hãy thử lại.", Toast.LENGTH_SHORT).show();
                    }
                    tabNumber = 0;
                }
                break;
            case R.id.tab_bus:
                if (tabNumber != 1) {
                    iconBus.setImageResource(R.drawable.ic_bus);
                    tvBus.setTextColor(Color.parseColor("#3F51B5"));
                    tabBus.setBackgroundResource(R.drawable.transit_bg_selected);

                    tabCar.setBackgroundResource(R.drawable.transit_bg_normal);
                    iconCar.setImageResource(R.drawable.ic_car_white);
                    tvCar.setTextColor(Color.WHITE);
                    tabWalking.setBackgroundResource(R.drawable.transit_bg_normal);
                    iconWalking.setImageResource(R.drawable.ic_walking_white);
                    tvWalking.setTextColor(Color.WHITE);
                    fabDirection.setVisibility(GONE);

                    if (currentLocation != null) {
                        mMap.clear();
                        mMap.addMarker(new MarkerOptions().position(currentLocation));
                        getDirection.getBusDirection(Util.createBusURL(this, currentLocation, new LatLng(latitude, longitude)), tvBus);
                    } else {
                        Toast.makeText(this, "Không lấy được vị trí. Hãy thử lại.", Toast.LENGTH_SHORT).show();
                    }
                    tabNumber = 1;
                }
                break;
            case R.id.tab_walking:
                if (tabNumber != 2) {
                    iconWalking.setImageResource(R.drawable.ic_walking);
                    tvWalking.setTextColor(Color.parseColor("#3F51B5"));
                    tabWalking.setBackgroundResource(R.drawable.transit_bg_selected);
                    tabBus.setBackgroundResource(R.drawable.transit_bg_normal);
                    iconBus.setImageResource(R.drawable.ic_bus_white);
                    tvBus.setTextColor(Color.WHITE);
                    tabCar.setBackgroundResource(R.drawable.transit_bg_normal);
                    iconCar.setImageResource(R.drawable.ic_car_white);
                    tvCar.setTextColor(Color.WHITE);
                    fabDirection.setVisibility(VISIBLE);

                    if (currentLocation != null) {
                        mMap.clear();
                        mMap.addMarker(new MarkerOptions().position(currentLocation));
                        getDirection.getWalkingDirection(Util.createWalkingURL(this, currentLocation, new LatLng(latitude, longitude)), tvWalking);
                    } else {
                        Toast.makeText(this, "Không lấy được vị trí. Hãy thử lại.", Toast.LENGTH_SHORT).show();
                    }

                    tabNumber = 2;
                }
                break;
            case R.id.fab_direct:
                String URL = "";
                if (tabNumber == 0) {
                    URL = Util.createCarURL(this, currentLocation, new LatLng(latitude, longitude));
                } else if (tabNumber == 2) {
                    URL = Util.createWalkingURL(this, currentLocation, new LatLng(latitude, longitude));
                }

                StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            Directions result = new Gson().fromJson(response, Directions.class);
                            Directions.Route[] routes = result.getRoutes();

                            if (routes != null) {
                                Intent routePreview = new Intent(FullMapActivity.this, RouteActivity.class);
                                routePreview.putExtra("route", new Gson().toJson(result));

                                if (tabNumber == 0) {
                                    routePreview.putExtra("mode", "car");
                                } else if (tabNumber == 2) {
                                    routePreview.putExtra("mode", "walking");
                                }

                                startActivity(routePreview);
                            } else Toast.makeText(FullMapActivity.this, result.getStatus(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(FullMapActivity.this, "Lấy chỉ đường thất bại. Hãy thử lại.", Toast.LENGTH_SHORT).show();
                            }
                        });
                VolleySingleton.getInstance(FullMapActivity.this).getRequestQueue().add(strReq);
                break;
        }
    }


    @Override
    public void onMapReady(GoogleMap map) {
        map.getUiSettings().setZoomControlsEnabled(false);
        map.getUiSettings().setCompassEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.getUiSettings().setAllGesturesEnabled(true);
        map.setOnPolylineClickListener(this);
        mMap = map;
        mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            locationPermissionStatus = getSharedPreferences("LOCATION_PERMISSION", MODE_PRIVATE);
            checkLocationPermission();
        } else {
            checkLocationServicedEnabled();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(FullMapActivity.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        requestLocation();

        if (ContextCompat.checkSelfPermission(FullMapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int paramInt) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        buildGoogleApiClient();
        Toast.makeText(this, "Lấy vị trí thất bại. Hãy thử lại.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {

            if (firstRun) {

                if (!fromFavorite) {
                    currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                }

                getDirection = new GetDirection(this, mMap, new LatLng(latitude, longitude), tvDistanceTime, steps, adapter);
                tabCar.performClick();
                getDirection.getWalkingDirection1(Util.createWalkingURL(this, currentLocation, new LatLng(latitude, longitude)), tvWalking);
                getDirection.getBusDirection1(Util.createBusURL(this, currentLocation, new LatLng(latitude, longitude)), tvBus);
                firstRun = false;
            }
            removeLocationUpdates();
        }
    }


    private void requestLocation() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(5000L);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setFastestInterval(1000);
    }

    private void removeLocationUpdates() {
        if (mGoogleApiClient != null) {

            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            }
            mGoogleApiClient.disconnect();
        }
    }

    private void checkLocationPermission() {
        if (ActivityCompat.checkSelfPermission(FullMapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(FullMapActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                Util.showLocationRequestDialog1(FullMapActivity.this, ACCESS_FINE_PERMISSION_CONSTANT);
            } else if (locationPermissionStatus.getBoolean(Manifest.permission.ACCESS_FINE_LOCATION, false)) {
                showLocationRequestDialog();
            } else {
                ActivityCompat.requestPermissions(FullMapActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        ACCESS_FINE_PERMISSION_CONSTANT);
            }
            SharedPreferences.Editor editor = locationPermissionStatus.edit();
            editor.putBoolean(Manifest.permission.ACCESS_FINE_LOCATION, true);
            editor.commit();
        } else {
            checkLocationServicedEnabled();
        }
    }

    private void checkLocationServicedEnabled() {
        if (!Util.isLocationServiceEnabled(getApplicationContext())) {
            showLocationRequestDialog();
        }
        else {
            if (ActivityCompat.checkSelfPermission(FullMapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                requestLocation();
                mMap.setMyLocationEnabled(true);
            }
        }
    }

    private void showLocationRequestDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(FullMapActivity.this);
        builder.setTitle("Cần quyền truy cập vị trí");
        builder.setMessage("Ứng dụng cần quyền truy cập vị trí.");
        builder.setPositiveButton("Cấp quyền", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(myIntent, REQUEST_FINE_PERMISSION_SETTING);
            }
        });
        builder.setNegativeButton("Huỷ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_FINE_PERMISSION_SETTING) {

            if (ActivityCompat.checkSelfPermission(FullMapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                requestLocation();
                mMap.setMyLocationEnabled(true);
            } else {
                Util.showLocationRequestDialog1(FullMapActivity.this, ACCESS_FINE_PERMISSION_CONSTANT);
            }
        }

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                tvOrigin.setText(place.getAddress());
                currentLocation = place.getLatLng();
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(place.getLatLng()));
                getDirection.getCarDirection(Util.createCarURL(this, currentLocation, new LatLng(latitude, longitude)), tvCar);
                getDirection.getCarDirection(Util.createBusURL(this, currentLocation, new LatLng(latitude, longitude)), tvBus);

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Toast.makeText(this, status.toString(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ACCESS_FINE_PERMISSION_CONSTANT) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if (ContextCompat.checkSelfPermission(FullMapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    checkLocationServicedEnabled();
                }

            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(FullMapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Util.showLocationRequestDialog1(FullMapActivity.this, ACCESS_FINE_PERMISSION_CONSTANT);
                } else {
                    Toast.makeText(getBaseContext(), "Không thể lấy quyền truy cập vị trí!", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onPolylineClick (Polyline polyline) {
        if (builder != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 200), 1000, null);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_full_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_reverse:
                String origin = tvOrigin.getText().toString();
                tvOrigin.setText(tvDestination.getText().toString());
                tvDestination.setText(origin);
                LatLng temp = currentLocation;
                currentLocation = new LatLng(latitude, longitude);
                latitude = temp.latitude;
                longitude = temp.longitude;
                getDirection = new GetDirection(this, mMap, new LatLng(latitude, longitude), tvDistanceTime, steps, adapter);
                tabCar.performClick();
                getDirection.getWalkingDirection1(Util.createWalkingURL(this, currentLocation, new LatLng(latitude, longitude)), tvWalking);
                getDirection.getBusDirection1(Util.createBusURL(this, currentLocation, new LatLng(latitude, longitude)), tvBus);
                break;
            case R.id.action_share_direction:
                String URL;
                if (tabNumber == 0) {
                    URL = Util.createCarURL(this, currentLocation, new LatLng(latitude, longitude));
                } else if (tabNumber == 2) {
                    URL = Util.createWalkingURL(this, currentLocation, new LatLng(latitude, longitude));
                } else {
                    URL = Util.createBusURL(this, currentLocation, new LatLng(latitude, longitude));
                }

                StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            Directions result = new Gson().fromJson(response, Directions.class);
                            Directions.Route[] routes = result.getRoutes();

                            if (routes != null) {
                                Directions.Leg[] leg = routes[0].getLegs();
                                Step[] steps = leg[0].getSteps();

                                StringBuilder stepStr = new StringBuilder();
                                stepStr.append("Chỉ đường từ " + leg[0].getStart_address() + " đến " + leg[0].getEnd_address() + "\n\n");

                                for (int i = 0; i < steps.length; i++) {
                                    if (steps[i].getHtml_instructions().indexOf("div") != -1) {
                                        stepStr.append(String.valueOf(i + 1) + ". " + Html.fromHtml(steps[i].getHtml_instructions().substring(0,
                                                steps[i].getHtml_instructions().indexOf("div") - 1)).toString() + "\n");
                                    } else {
                                        stepStr.append(String.valueOf(i + 1) + ". " + Html.fromHtml(steps[i].getHtml_instructions()).toString() + "\n");
                                    }
                                }

                                String str = stepStr.toString();

                                Intent shareIntent = new Intent();
                                shareIntent.setAction("android.intent.action.SEND");
                                shareIntent.putExtra("android.intent.extra.TEXT", str);
                                shareIntent.setType("text/plain");
                                startActivity(Intent.createChooser(shareIntent, "Chia sẻ"));

                            } else Toast.makeText(FullMapActivity.this, result.getStatus(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(FullMapActivity.this, "Lấy chỉ đường thất bại. Hãy thử lại.", Toast.LENGTH_SHORT).show();
                            }
                        });
                VolleySingleton.getInstance(FullMapActivity.this).getRequestQueue().add(strReq);
                break;
            case R.id.action_map_normal:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case R.id.action_map_satellite:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.action_map_terrain:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
           behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        removeLocationUpdates();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
    }
}
