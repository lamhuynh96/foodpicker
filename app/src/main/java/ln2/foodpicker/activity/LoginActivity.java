package ln2.foodpicker.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import ln2.foodpicker.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import ln2.foodpicker.model.User;
import ln2.foodpicker.util.CurrentUserSingleton;
import ln2.foodpicker.util.SessionManager;
import ln2.foodpicker.util.Util;
import ln2.foodpicker.util.VolleySingleton;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.input_email)
    EditText edtEmail;
    @BindView(R.id.input_password)
    EditText edtPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.link_signup)
    TextView txvLinkSignup;

    SessionManager session;
    private static final int REQUEST_SIGNUP = 0;
    private Gson gson;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        ButterKnife.bind(this);

        session = new SessionManager(getApplicationContext());

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogin();
            }
        });

        txvLinkSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
            }
        });
    }

    public void doLogin() {
        if (!validate()) {
            btnLogin.setEnabled(true);
            return;
        }

        progressDialog = Util.showDialog("Đang đăng nhập", this);

        final String email = edtEmail.getText().toString();
        final String password = edtPassword.getText().toString();

        String URL = getString(R.string.login_check_url);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        if (!jsonObject.getBoolean("success")) {
                            Toast.makeText(LoginActivity.this,
                                    "Email hoặc mất khẩu không chính xác", Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            JSONArray jsonArr = jsonObject.getJSONArray("payload");
                            JSONObject userJson = jsonArr.getJSONObject(0);
                            CurrentUserSingleton.setData(userJson.getString("userId"), userJson.getString("userName"),
                                    userJson.getString("password"), userJson.getString("email"), userJson.getString("image_path"));
                            User currentUser = CurrentUserSingleton.getCurrentUser();
                            session.createLoginSession(currentUser.getUserId(),
                                    currentUser.getUserName(),
                                    currentUser.getEmail(),
                                    currentUser.getPassword(),
                                    currentUser.getImage_path());

                            Toast.makeText(LoginActivity.this,
                                    "Đăng nhập với tên " + currentUser.getUserName(), Toast.LENGTH_SHORT).show();

                            Intent i = new Intent(getApplicationContext(), FoodActivity.class);
                            startActivity(i);
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(LoginActivity.this,
                            "Không thể kết nối Server, xin hãy thử lại!", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };

        VolleySingleton.getInstance(this).getRequestQueue().add(stringRequest);
    }

    public Boolean validate() {
        boolean valid = true;

        String email = edtEmail.getText().toString();
        String password = edtPassword.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            edtEmail.setError("Xin hãy nhập email hợp lệ");
            valid = false;
        } else {
            edtEmail.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 20) {
            edtPassword.setError("Mật khẩu có 8 đến 20 kí tự");
            valid = false;
        } else {
            edtPassword.setError(null);
        }

        return valid;
    }
}
