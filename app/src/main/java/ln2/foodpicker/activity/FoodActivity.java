package ln2.foodpicker.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import ln2.foodpicker.R;

import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lapism.searchview.SearchAdapter;
import com.lapism.searchview.SearchItem;
import com.lapism.searchview.SearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import ln2.foodpicker.adapter.CircleTransform;
import ln2.foodpicker.adapter.CustomSearchAdapter;
import ln2.foodpicker.adapter.FeedListAdapter;
import ln2.foodpicker.model.FoodStall;
import ln2.foodpicker.util.CurrentUserSingleton;
import ln2.foodpicker.util.NotificationUtil;
import ln2.foodpicker.util.SessionManager;
import ln2.foodpicker.util.Util;
import ln2.foodpicker.util.VolleySingleton;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.bumptech.glide.load.engine.DiskCacheStrategy.NONE;

public class FoodActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnMenuClickListener,
        AdapterView.OnItemClickListener, SearchView.OnQueryTextListener, CustomSearchAdapter.OnItemClickListener, View.OnClickListener {

    private static final int IMG_PROFILE_CAMERA_REQUEST_CODE = 1;
    private static final int IMG_PROFILE_GALLERY_REQUEST_CODE = 2;
    private ActionBarDrawerToggle toggle;
    private FeedListAdapter feedListAdapter;
    private ArrayList<FoodStall> stalls;
    private List<SearchItem> keywordList;
    private ProgressDialog progressDialog;
    private Gson gson;
    private TextView txvName, txvEmail;
    private ImageView imgProfile;
    private SessionManager session;
    private HashMap<String, String> userInfor;
    private View navHeader;
    private ProgressBar progressBar;
    private CustomSearchAdapter searchAdapter;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view_food) NavigationView navigationView;
    @BindView(R.id.layout_food) RelativeLayout layoutFood;
    @BindView(R.id.list_food) ListView listFood;
    @BindView(R.id.search_view) SearchView searchView;
    @BindView(R.id.notification) FrameLayout notification;
    @BindView(R.id.tv_noti_number) TextView tvNotificationNumber;
    @BindView(R.id.layout_try_again) LinearLayout layoutTryAgain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);
        ButterKnife.bind(this);
        session = new SessionManager(getApplicationContext());

        if (session.checkLogin()) {
            finish();
            return;
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        addEvents();
    }

    private void addEvents() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        toggle = new ActionBarDrawerToggle(this, drawer, toolbar,  R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (searchView != null && searchView.isSearchOpen()) {
                    searchView.close(true);
                }
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        toggle.syncState();
        drawer.addDrawerListener(toggle);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        tvNotificationNumber.setVisibility(GONE);
        navHeader = navigationView.getHeaderView(0);
        txvName = (TextView) navHeader.findViewById(R.id.tv_userName);
        txvEmail = (TextView) navHeader.findViewById(R.id.tv_email);
        imgProfile = (ImageView) navHeader.findViewById(R.id.img_profile);
        progressBar = (ProgressBar) navHeader.findViewById(R.id.img_progress_bar);

        userInfor = session.getUserDetails();
        CurrentUserSingleton.setData(userInfor.get(SessionManager.KEY_ID),
                userInfor.get(SessionManager.KEY_NAME),
                userInfor.get(SessionManager.KEY_PASSWORD),
                userInfor.get(SessionManager.KEY_EMAIL),
                userInfor.get(SessionManager.KEY_IMAGE));

        txvName.setText(userInfor.get(SessionManager.KEY_NAME));
        txvEmail.setText(userInfor.get(SessionManager.KEY_EMAIL));

        if (userInfor.get(SessionManager.KEY_IMAGE).equals("")) {
            Glide.with(this).load(R.drawable.logo)
                    .crossFade()
                    .thumbnail(0.5f)
                    .diskCacheStrategy(NONE)
                    .skipMemoryCache(true)
                    .bitmapTransform(new CircleTransform(this))
                    .into(imgProfile);

        } else {
            Glide.with(this).load(userInfor.get(SessionManager.KEY_IMAGE))
                    .crossFade()
                    .thumbnail(0.5f)
                    .diskCacheStrategy(NONE)
                    .skipMemoryCache(true)
                    .bitmapTransform(new CircleTransform(this))
                    .into(imgProfile);

        }

        stalls = new ArrayList<>();
        feedListAdapter = new FeedListAdapter(this, stalls);
        listFood.setAdapter(feedListAdapter);
        gson = new GsonBuilder().create();
        setUpSearchView();
        listFood.setOnItemClickListener(this);
        progressDialog = Util.showDialog("Đang tải danh sách địa điểm ...", this);

        getKeywordList();
        getFeedList("");
        getUnseenNotificationNumber(getString(R.string.get_unseen_noti_number) +
                new SessionManager(this).getUserDetails().get(SessionManager.KEY_ID));

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openPopup(view);
            }
        });

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("pushNotification")) {
                    String message = intent.getStringExtra("message");
                    getUnseenNotificationNumber(getString(R.string.get_unseen_noti_number) +
                            new SessionManager(FoodActivity.this).getUserDetails().get(SessionManager.KEY_ID));
                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };

        SharedPreferences pref = getApplicationContext().getSharedPreferences("firebase", 0);
        String regId = pref.getString("regId", null);
        sendRegistrationToServer(regId);
        findViewById(R.id.btn_try_again).setOnClickListener(this);
        notification.setOnClickListener(this);
    }

    private void setUpSearchView() {
        searchView.setHint("Nhập từ khoá ...");
        searchView.setOnQueryTextListener(this);
        searchView.setVoice(false);

        keywordList = new ArrayList<>();
        searchAdapter = new CustomSearchAdapter(this, keywordList);
        searchAdapter.addOnItemClickListener(this);
        searchView.setAdapter(searchAdapter);
        searchView.setOnMenuClickListener(this);
        searchView.setShadow(false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.notification:
                Intent notification = new Intent(this, NotificationActivity.class);
                startActivity(notification);
                break;
            case R.id.btn_try_again:
                getFeedList("");
                break;
        }
    }

    @Override
    public void onMenuClick() {
        drawer.openDrawer(GravityCompat.START);
    }

    @Override
    public void onItemClick(View view, int i) {
        TextView textView = (TextView) view.findViewById(R.id.textView_item_text);
        getFeedList(textView.getText().toString());
        searchView.setTextOnly(textView.getText().toString());
        searchView.close(false);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        getFeedList(query);

        if (notification.getVisibility() == GONE) {
            notification.setVisibility(VISIBLE);
        }
        searchView.close(false);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText.equals("")) {

            if (notification.getVisibility() == GONE) {
                notification.setVisibility(VISIBLE);
            }
            getFeedList("");
        }

        if (!newText.equals("") && notification.getVisibility() == VISIBLE) {
            notification.setVisibility(GONE);
        }
        return true;
    }

    private void openPopup(View view) {
        PopupMenu menu = new PopupMenu(this, view);
        menu.getMenuInflater().inflate(R.menu.menu_img_profile, menu.getMenu());
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.action_camera:
                        Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(camera, IMG_PROFILE_CAMERA_REQUEST_CODE);
                        return true;
                    case R.id.action_gallery:
                        Intent galleryPicker = new Intent(Intent.ACTION_PICK);
                        galleryPicker.setType("image/*");
                        startActivityForResult(galleryPicker, IMG_PROFILE_GALLERY_REQUEST_CODE);
                        return true;
                    default:
                        return false;
                }
            }
        });
        menu.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent returnedIntent) {
        super.onActivityResult(requestCode, resultCode, returnedIntent);
        Uri imageUri;
        switch (requestCode) {
            case IMG_PROFILE_CAMERA_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) returnedIntent.getExtras().get("data");
                    updateImageProfile(Util.saveImage(this, photo));
                    progressBar.setVisibility(VISIBLE);
                    }
                break;
            case IMG_PROFILE_GALLERY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    imageUri = returnedIntent.getData();
                    updateImageProfile(Util.saveImage(this, imageUri));
                    progressBar.setVisibility(VISIBLE);
                }
                break;
        }
    }

    private void updateImageProfile(final String base64) {
        StringRequest strReq = new StringRequest(Request.Method.POST, getString(R.string.update_img_profile), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(GONE);
                if (response != null) {
                    Log.i("response", response);

                    try {
                        JSONObject res = new JSONObject(response);
                        JSONObject payload = res.getJSONObject("payload");
                        String image_path = payload.getString("avatar");
                        new SessionManager(FoodActivity.this).updateImagePath(image_path);
                        Log.i("image_path", image_path);

                        Glide.with(FoodActivity.this)
                                .load(image_path)
                                .crossFade()
                                .thumbnail(0.5f)
                                .diskCacheStrategy(NONE)
                                .skipMemoryCache( true )
                                .bitmapTransform(new CircleTransform(FoodActivity.this))
                                .into(imgProfile);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBar.setVisibility(GONE);
                        Toast.makeText(FoodActivity.this, "Cập nhật hình thất bại. Hãy thử lại.", Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("userId", userInfor.get(SessionManager.KEY_ID));
                params.put("base64", base64);

                return params;
            }
        };
        VolleySingleton.getInstance(this).getRequestQueue().add(strReq);
    }

    private void sendRegistrationToServer(final String token) {
        StringRequest strReq = new StringRequest(Request.Method.POST, getString(R.string.send_token), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {}
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {}
                }) {
            @Override
            protected Map<String, String> getParams () {
                Map<String, String> params = new HashMap<>();
                params.put("userId", new SessionManager(getApplicationContext()).getUserDetails().get(SessionManager.KEY_ID));
                params.put("token", token);
                return params;
            }
        };
        VolleySingleton.getInstance(this).getRequestQueue().add(strReq);
    }

    public void getFeedList(String keyword) {
        String URL;
        if (keyword.equals("")) {
            URL = getString(R.string.food_load_all_url);
        } else URL = getString(R.string.food_keyword_url) + keyword;

        StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                listFood.setVisibility(VISIBLE);
                layoutTryAgain.setVisibility(GONE);

                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("payload");
                        List result = Arrays.asList(gson.fromJson(jsonArray.toString(), FoodStall[].class));
                        stalls.clear();
                        stalls.addAll(result);
                        feedListAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        listFood.setVisibility(GONE);
                        layoutTryAgain.setVisibility(VISIBLE);
                    }
                });
        VolleySingleton.getInstance(this).getRequestQueue().add(strReq);
    }

    public void getUnseenNotificationNumber(final String URL) {
        StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        JSONObject res = new JSONObject(response);

                        if (res.getInt("payload") > 0) {
                            tvNotificationNumber.setVisibility(VISIBLE);
                            tvNotificationNumber.setText(String.valueOf(res.getInt("payload")));
                        } else {
                            tvNotificationNumber.setVisibility(GONE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        getUnseenNotificationNumber(URL);
                    }
                });
        VolleySingleton.getInstance(this).getRequestQueue().add(strReq);
    }

    public void getKeywordList() {
        StringRequest strReq = new StringRequest(Request.Method.GET, getString(R.string.get_keyword_list), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject res = new JSONObject(response);
                        String keywords = res.getString("payload");
                        String []tokens = keywords.split(", ");

                        for (String token : tokens) {
                            keywordList.add(new SearchItem(token));
                        }

                        searchAdapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Snackbar snackbar = Snackbar
                                .make(layoutFood, "Tải danh sách từ khoá thất bại", Snackbar.LENGTH_LONG)
                                .setAction("Thử lại", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        getKeywordList();
                                    }
                                });
                        snackbar.setActionTextColor(Color.WHITE);
                        snackbar.show();
                    }
                });
        VolleySingleton.getInstance(this).getRequestQueue().add(strReq);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        FoodStall stall = feedListAdapter.getItem(position);
        Intent detail = new Intent(this, DetailActivity.class);
        detail.putExtra("detail", gson.toJson(stall));
        startActivity(detail);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_around:
                Intent searchAround = new Intent(this, SearchAroundActivity.class);
                startActivity(searchAround);
                break;
            case R.id.nav_favorite:
                Intent favorite = new Intent(this, FavoriteActivity.class);
                startActivity(favorite);
                break;
            case R.id.nav_logout:
                session.logoutUser();
                finish();
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter("pushNotification"));
        NotificationUtil.clearNotifications(getApplicationContext());

        getUnseenNotificationNumber(getString(R.string.get_unseen_noti_number) +
                new SessionManager(this).getUserDetails().get(SessionManager.KEY_ID));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
