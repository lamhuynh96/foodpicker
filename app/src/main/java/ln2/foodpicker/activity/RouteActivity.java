package ln2.foodpicker.activity;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;
import com.google.gson.Gson;
import com.google.maps.android.PolyUtil;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ln2.foodpicker.R;
import ln2.foodpicker.model.Directions;
import ln2.foodpicker.model.Directions.Leg.Step;
import ln2.foodpicker.model.Directions.Leg;
import ln2.foodpicker.model.Directions.Route;

public class RouteActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {

    private Directions result;
    private GoogleMap mMap;
    private Step[] steps;
    private String mode;
    private String encodedPoints;
    private Marker marker;
    private Leg[] leg;
    private int position = 0;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tv_distance) TextView tvDistance;
    @BindView(R.id.tv_instruction) TextView tvInstruction;
    @BindView(R.id.btn_next) ImageView btnNext;
    @BindView(R.id.btn_prev) ImageView btnPrev;
    @BindView(R.id.img_manuver) ImageView imgManeuver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);
        ButterKnife.bind(this);
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        addEvents();
    }

    private void addEvents() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Chỉ đường");
        btnNext.setOnClickListener(this);
        btnPrev.setOnClickListener(this);

        result = new Gson().fromJson(getIntent().getExtras().getString("route"), Directions.class);
        mode = getIntent().getExtras().getString("mode");
        Route[] routes = result.getRoutes();
        leg = routes[0].getLegs();
        encodedPoints = routes[0].getOverview_polyline().getPoints();
        steps = leg[0].getSteps();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.getUiSettings().setZoomControlsEnabled(false);
        map.getUiSettings().setCompassEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.getUiSettings().setAllGesturesEnabled(true);
        mMap = map;
        addPolyline();
        setUpInstruction(steps[0].getDistance().getText(), steps[0].getHtml_instructions(), steps[0].getManeuver());
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.btn_next:
                position++;

                if (position > steps.length - 1) {
                    position = steps.length;

                    if (marker != null) {
                        marker.remove();
                    }

                    setUpInstruction("", leg[0].getEnd_address(), null);
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(PolyUtil.decode(encodedPoints).get(PolyUtil.decode(encodedPoints).size() - 1), 18.0F));

                } else {
                    setUpInstruction(steps[position].getDistance().getText(), steps[position].getHtml_instructions(), steps[position].getManeuver());

                    if (steps[position].getManeuver() != null)
                        Log.i("manuver", steps[position].getManeuver());
                    if (marker != null) {
                        marker.remove();
                    }

                    marker = mMap.addMarker(new MarkerOptions().position(new LatLng(steps[position].getStart_location().getLat(), steps[position].getStart_location().getLng())));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(steps[position].getStart_location().getLat(), steps[position].getStart_location().getLng()), 18.0F));
                }
                break;
            case R.id.btn_prev:
                position--;

                if (position < 0) {
                    position = 0;
                } else {
                    setUpInstruction(steps[position].getDistance().getText(), steps[position].getHtml_instructions(), steps[position].getManeuver());
                    if (steps[position].getManeuver() != null)
                        Log.i("manuver", steps[position].getManeuver());
                    if (marker != null) {
                        marker.remove();
                    }

                    marker = mMap.addMarker(new MarkerOptions().position(new LatLng(steps[position].getStart_location().getLat(), steps[position].getStart_location().getLng())));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(steps[position].getStart_location().getLat(), steps[position].getStart_location().getLng()), 18.0F));
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpInstruction(String distance, String instruction, String maneuver) {
        tvDistance.setText(distance);

        if (instruction.indexOf("div") != -1) {
            tvInstruction.setText(Html.fromHtml(instruction.substring(0, instruction.indexOf("div") - 1)));
        } else {
            tvInstruction.setText(Html.fromHtml(instruction));
        }

        if (maneuver != null) {
            switch(maneuver) {
                case "turn-left":
                    imgManeuver.setImageResource(R.drawable.ic_turn_left);
                    break;
                case "turn-right":
                    imgManeuver.setImageResource(R.drawable.ic_turn_right);
                    break;
                case "roundabout-right":
                    imgManeuver.setImageResource(R.drawable.ic_round_about_right);
                    break;
                case "roundabout-left":
                    imgManeuver.setImageResource(R.drawable.ic_round_about_left);
                    break;
                case "straight":
                    imgManeuver.setImageResource(R.drawable.ic_straight);
                    break;
            }
        } else imgManeuver.setImageResource(R.drawable.ic_info);
    }

    private void addPolyline() {
        if (mode.equals("car")) {
            mMap.addMarker(new MarkerOptions().position(PolyUtil.decode(encodedPoints).get(0)));
            mMap.addMarker(new MarkerOptions().position(PolyUtil.decode(encodedPoints).get(PolyUtil.decode(encodedPoints).size() - 1)));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(PolyUtil.decode(encodedPoints).get(0), 16.0F));
            mMap.addPolyline(new PolylineOptions().addAll(PolyUtil.decode(encodedPoints))
                    .width(20.0F)
                    .color(Color.parseColor("#3F51B5"))
                    .startCap(new RoundCap())
                    .endCap(new RoundCap())
                    .pattern(null)
                    .geodesic(true)
                    .clickable(true)
            );
        } else {
            List<PatternItem> listPattern = Arrays.<PatternItem>asList(new Dot());
            mMap.addMarker(new MarkerOptions().position(PolyUtil.decode(encodedPoints).get(0)));
            mMap.addMarker(new MarkerOptions().position(PolyUtil.decode(encodedPoints).get(PolyUtil.decode(encodedPoints).size() - 1)));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(PolyUtil.decode(encodedPoints).get(0), 16.0F));
            mMap.addPolyline(new PolylineOptions().addAll(PolyUtil.decode(encodedPoints))
                    .width(20.0F)
                    .color(Color.parseColor("#3F51B5"))
                    .startCap(new RoundCap())
                    .endCap(new RoundCap())
                    .pattern(null)
                    .geodesic(true)
                    .clickable(true)
            ).setPattern(listPattern);
        }
    }
}
