package ln2.foodpicker.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ln2.foodpicker.R;
import ln2.foodpicker.adapter.NotificationListAdapter;
import ln2.foodpicker.model.FoodStall;
import ln2.foodpicker.model.Notification;
import ln2.foodpicker.util.SessionManager;
import ln2.foodpicker.util.VolleySingleton;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class NotificationActivity extends AppCompatActivity
        implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    private ArrayList<Notification> notifications;
    private NotificationListAdapter adapter;

    @BindView(R.id.layout_swipe) SwipeRefreshLayout layoutSwipe;
    @BindView(R.id.notification_list) ListView notificationList;
    @BindView(R.id.layout_try_again) LinearLayout layoutTryAgain;
    @BindView(R.id.layout_empty) LinearLayout layoutEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        addEvents();
    }

    private void addEvents() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Thông báo");

        notifications = new ArrayList<>();
        adapter = new NotificationListAdapter(this, notifications);
        notificationList.setAdapter(adapter);
        layoutSwipe.setOnRefreshListener(this);
        findViewById(R.id.btn_try_again).setOnClickListener(this);
        getNotificationList(getString(R.string.get_notifications) + new SessionManager(this).getUserDetails().get(SessionManager.KEY_ID));
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_try_again) {
            getNotificationList(getString(R.string.get_notifications) + new SessionManager(this).getUserDetails().get(SessionManager.KEY_ID));
        }
    }

    @Override
    public void onRefresh() {
        getNotificationList(getString(R.string.get_notifications) + new SessionManager(this).getUserDetails().get(SessionManager.KEY_ID));
    }

    private void getNotificationList(String URL) {
        layoutSwipe.setRefreshing(true);

        StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                notificationList.setVisibility(VISIBLE);
                layoutTryAgain.setVisibility(GONE);

                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("payload");
                        List result = Arrays.asList(new Gson().fromJson(jsonArray.toString(), Notification[].class));

                        if (result.isEmpty()) {
                            notificationList.setVisibility(GONE);
                            layoutTryAgain.setVisibility(GONE);
                            layoutEmpty.setVisibility(VISIBLE);
                            return;
                        }

                        notifications.clear();
                        notifications.addAll(result);
                        adapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                layoutSwipe.setRefreshing(false);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        layoutSwipe.setRefreshing(false);
                        notificationList.setVisibility(GONE);
                        layoutTryAgain.setVisibility(VISIBLE);
                    }
                });
        VolleySingleton.getInstance(this).getRequestQueue().add(strReq);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
