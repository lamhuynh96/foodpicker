package ln2.foodpicker.model;

public final class Directions {
    public final Route routes[];
    public final String status;

    public Route[] getRoutes() {
        return routes;
    }
    public String getStatus() {
        return status;
    }

    public Directions(Route[] routes, String status){
        this.routes = routes;
        this.status = status;
    }

    public static final class Route {
        public final Leg legs[];

        public final Overview_polyline overview_polyline;

        public Leg[] getLegs() {
            return legs;
        }

        public Overview_polyline getOverview_polyline() {
            return overview_polyline;
        }

        public Route(Leg[] legs, Overview_polyline overview_polyline){
            this.legs = legs;
            this.overview_polyline = overview_polyline;
        }
    }

    public static final class Overview_polyline {
        public final String points;

        public Overview_polyline(String points) {
            this.points = points;
        }

        public String getPoints() {
            return points;
        }

    }

    public static final class Leg {
        public final String end_address;
        public final End_location end_location;
        public final String start_address;
        public final Start_location start_location;
        public final Distance distance;
        public final Duration duration;
        public final Step steps[];

        public String getEnd_address() {
            return end_address;
        }

        public End_location getEnd_location() {
            return end_location;
        }

        public String getStart_address() {
            return start_address;
        }

        public Start_location getStart_location() {
            return start_location;
        }

        public Step[] getSteps() {
            return steps;
        }

        public Distance getDistance() {
            return distance;
        }

        public Duration getDuration() {
            return duration;
        }

        public Leg(Distance distance, Duration duration, String end_address, End_location end_location,
                   String start_address, Start_location start_location, Step[] steps){
            this.distance = distance;
            this.duration = duration;
            this.end_address = end_address;
            this.end_location = end_location;
            this.start_address = start_address;
            this.start_location = start_location;
            this.steps = steps;
        }

        public static final class End_location {
            public final double lat;
            public final double lng;

            public End_location(double lat, double lng){
                this.lat = lat;
                this.lng = lng;
            }

            public double getLat() {
                return lat;
            }

            public double getLng() {
                return lng;
            }
        }

        public static final class Start_location {
            public final double lat;
            public final double lng;

            public Start_location(double lat, double lng){
                this.lat = lat;
                this.lng = lng;
            }

            public double getLat() {
                return lat;
            }
            public double getLng() {
                return lng;
            }

        }

        public static final class Step {
            public final End_location end_location;
            public final String html_instructions;
            public final Start_location start_location;
            public final Distance distance;
            public final String travel_mode;
            public final String maneuver;
            public final Transit_Detail transit_details;
            public final Overview_polyline polyline;

            public Overview_polyline getPolyline() {
                return polyline;
            }

            public End_location getEnd_location() {
                return end_location;
            }

            public String getHtml_instructions() {
                return html_instructions;
            }

            public Start_location getStart_location() {
                return start_location;
            }

            public String getTravel_mode() {
                return travel_mode;
            }

            public String getManeuver() {
                return maneuver;
            }

            public Distance getDistance() {
                return distance;
            }

            public Transit_Detail getTransit_detail() {
                return transit_details;
            }

            public Step(Distance distance, End_location end_location, String html_instructions, Start_location start_location,
                        String travel_mode, String maneuver, Transit_Detail transit_details, Overview_polyline polyline){
                this.distance = distance;
                this.end_location = end_location;
                this.html_instructions = html_instructions;
                this.start_location = start_location;
                this.polyline = polyline;
                this.transit_details = transit_details;
                this.travel_mode = travel_mode;
                this.maneuver = maneuver;
            }
        }

        public static final class Distance {
            public final String text;
            public final Long value;

            public String getText() {
                return text;
            }

            public Long getValue() {
                return value;
            }

            public Distance(String text, Long value) {
                this.text = text;
                this.value = value;
            }
        }

        public static final class Duration {
            public final String text;
            public final Long value;

            public String getText() {
                return text;
            }

            public Long getValue() {
                return value;
            }
            public Duration(String text, Long value) {
                this.text = text;
                this.value = value;
            }
        }

        public static final class Transit_Detail {
            public final Arrival_Stop arrival_stop;
            public final Arrival_Stop departure_stop;
            public final Line line;
            public final int num_stops;

            public int getNum_stops() {
                return num_stops;
            }

            public Arrival_Stop getArrival_stop() {
                return arrival_stop;
            }

            public Arrival_Stop getDeparture_stop() {
                return departure_stop;
            }


            public Line getLine() {
                return line;
            }

            public Transit_Detail(Arrival_Stop arrival_stop, Arrival_Stop departure_stop, Line line, Integer num_stops) {
                this.arrival_stop = arrival_stop;
                this.departure_stop = departure_stop;
                this.line = line;
                this.num_stops = num_stops;
            }
        }

        public static final class Arrival_Stop {
            public final String name;
            public final Start_location location;

            public String getName() {
                return name;
            }

            public Start_location getLocation() {
                return location;
            }

            public Arrival_Stop(Start_location location, String name) {
                this.location = location;
                this.name = name;
            }
        }

        public static final class Line {
            public final String name;

            public String getName() {
                return name;
            }

            public Line(String name) {
                this.name = name;
            }
        }
    }
}