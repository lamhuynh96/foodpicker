package ln2.foodpicker.model;

public class Comment {
    String userName;
    String cmt;
    String rating;
    String image_path;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCmt() {
        return cmt;
    }

    public void setCmt(String cmt) {
        this.cmt = cmt;
    }

    public String getRating() {
        return String.valueOf(Math.round(Float.valueOf(rating)));
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }
}
