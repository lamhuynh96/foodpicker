package ln2.foodpicker.model;

public class User {
    private String userId;
    private String userName;
    private String password;
    private String email;
    private String image_path;

    public User() {}

    public User(String userId, String userName, String password, String email, String image_path) {
        this.userId = userId;
        this.userName = userName;
        this.password = password;
        this.email = email;
        this.image_path = image_path;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setUserId(String id) {
        this.userId = id;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }
}
