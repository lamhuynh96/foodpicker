package ln2.foodpicker.model;

import java.util.Objects;

public class FoodStall {
    String stallId;
    String stallName;
    String latitude;
    String longitude;
    String district;
    String descr;
    String price;
    String keyword;
    String openhour;
    String closehour;
    String address;
    String image_path;

    public String getStallId() {
        return stallId;
    }

    public void setStallId(String stallId) {
        this.stallId = stallId;
    }

    public String getStallName() {
        return stallName;
    }

    public void setStallName(String stallName) {
        this.stallName = stallName;
    }

    public Double getLatitude() {
        return Double.valueOf(latitude);
    }

    public void setLatitude(Double latitude) {
        this.latitude = String.valueOf(latitude);
    }

    public Double getLongitude() {
        return Double.valueOf(longitude);
    }

    public void setLongitude(Double longitude) {
        this.longitude = String.valueOf(longitude);
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getOpenhour() {
        return openhour;
    }

    public void setOpenhour(String openhour) {
        this.openhour = openhour;
    }

    public String getClosehour() {
        return closehour;
    }

    public void setClosehour(String closehour) {
        this.closehour = closehour;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;

        if (!(o instanceof FoodStall)) {
            return false;
        }

        FoodStall stall = (FoodStall) o;
        return Objects.equals(stallId, stall.stallId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stallId);
    }
}
