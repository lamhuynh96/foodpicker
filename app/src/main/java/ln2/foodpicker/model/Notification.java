package ln2.foodpicker.model;

public class Notification {
    String notifyId;
    String title;
    String message;
    String time_notify;
    int isSeen;
    FoodStall stallInfo;

    public String getNotifyId() {
        return notifyId;
    }

    public void setNotifyId(String notifyId) {
        this.notifyId = notifyId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime_notify() {
        return time_notify;
    }

    public void setTime_notify(String time_notify) {
        this.time_notify = time_notify;
    }

    public int getIsSeen() {
        return isSeen;
    }

    public void setIsSeen(int isSeen) {
        this.isSeen = isSeen;
    }

    public FoodStall getStallInfo() {
        return stallInfo;
    }

    public void setStallInfo(FoodStall stallInfo) {
        this.stallInfo = stallInfo;
    }
}
