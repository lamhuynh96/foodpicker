package ln2.foodpicker.asynctask;

import android.content.Context;
import ln2.foodpicker.R;
import android.graphics.Color;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;
import com.google.gson.Gson;
import com.google.maps.android.PolyUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ln2.foodpicker.adapter.InstructionListAdapter;
import ln2.foodpicker.model.Directions.Leg.Step;
import ln2.foodpicker.model.Directions;
import ln2.foodpicker.util.VolleySingleton;

public class GetDirection {
    private Context context;
    private GoogleMap mMap;
    private LatLng destination;
    private TextView tvDistanceTime;
    private LatLngBounds.Builder builder;
    private ArrayList<Step> steps;
    private InstructionListAdapter adapter;

    public GetDirection(Context context, GoogleMap mMap, LatLng destination, TextView tvDistanceTime,
                        ArrayList<Step> steps, InstructionListAdapter adapter) {
        this.context = context;
        this.mMap = mMap;
        this.destination = destination;
        this.tvDistanceTime = tvDistanceTime;
        this.steps = steps;
        this.adapter = adapter;
    }

    private void addCarDirection(String encodedPoints, String duration) {
        builder = new LatLngBounds.Builder();
        mMap.addMarker(new MarkerOptions().position(destination));
        mMap.addPolyline(new PolylineOptions().addAll(PolyUtil.decode(encodedPoints))
                .width(20.0F)
                .color(Color.parseColor("#3F51B5"))
                .startCap(new RoundCap())
                .endCap(new RoundCap())
                .pattern(null)
                .geodesic(true)
                .clickable(true)
        );

        BitmapDescriptor transparent = BitmapDescriptorFactory.fromResource(R.drawable.ic_transparent);
        MarkerOptions options = new MarkerOptions()
                .position(PolyUtil.decode(encodedPoints).get(PolyUtil.decode(encodedPoints).size() / 2))
                .title(duration)
                .icon(transparent)
                .anchor(0.5F, 0.5F);

        mMap.addMarker(options).showInfoWindow();

        for (LatLng latLng : PolyUtil.decode(encodedPoints)) {
            builder.include(latLng);
        }

        builder.build();
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 200), 1000, null);
    }

    private void addBusDirection(Directions.Route[] routes) {
        builder = new LatLngBounds.Builder();
        mMap.addMarker(new MarkerOptions().position(destination));

            Directions.Leg[] leg = routes[0].getLegs();
            for (int i = 0; i < leg.length; i++) {
                Step[] step = leg[i].getSteps();

               for (int j = 0; j < step.length; j++) {
                   if (step[j].getTravel_mode().equals("TRANSIT")) {
                       mMap.addPolyline(new PolylineOptions().addAll(PolyUtil.decode(step[j].getPolyline().getPoints()))
                               .width(20.0F)
                               .color(Color.parseColor("#3F51B5"))
                               .startCap(new RoundCap())
                               .endCap(new RoundCap())
                               .pattern(null)
                               .geodesic(true)
                               .clickable(true));
                   } else {
                       List<PatternItem> listPattern = Arrays.<PatternItem>asList(new Dot());
                       mMap.addPolyline(new PolylineOptions().addAll(PolyUtil.decode(step[j].getPolyline().getPoints()))
                               .width(20.0F)
                               .color(Color.parseColor("#3F51B5"))
                               .startCap(new RoundCap())
                               .endCap(new RoundCap())
                               .pattern(null)
                               .geodesic(true)
                               .clickable(true)
                       ).setPattern(listPattern);
                   }
            }
        }

        BitmapDescriptor transparent = BitmapDescriptorFactory.fromResource(R.drawable.ic_transparent);
        MarkerOptions options = new MarkerOptions()
                .position(PolyUtil.decode(routes[0].getOverview_polyline().points).get(PolyUtil.decode(routes[0].getOverview_polyline().points).size() / 2))
                .title(routes[0].getLegs()[0].getDuration().getText())
                .icon(transparent)
                .anchor(0.5F, 0.5F);

        mMap.addMarker(options).showInfoWindow();

        for (LatLng latLng : PolyUtil.decode(routes[0].getOverview_polyline().points)) {
            builder.include(latLng);
        }

        builder.build();
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 200), 1000, null);
    }

    private void addWalkingDirection(String encodedPoints, String duration) {
        builder = new LatLngBounds.Builder();
        mMap.addMarker(new MarkerOptions().position(destination));
        List<PatternItem> listPattern = Arrays.<PatternItem>asList(new Dot());
        mMap.addPolyline(new PolylineOptions().addAll(PolyUtil.decode(encodedPoints))
                .width(20.0F)
                .color(Color.parseColor("#3F51B5"))
                .startCap(new RoundCap())
                .endCap(new RoundCap())
                .pattern(null)
                .geodesic(true)
                .clickable(true)
        ).setPattern(listPattern);

        BitmapDescriptor transparent = BitmapDescriptorFactory.fromResource(R.drawable.ic_transparent);
        MarkerOptions options = new MarkerOptions()
                .position(PolyUtil.decode(encodedPoints).get(PolyUtil.decode(encodedPoints).size() / 2))
                .title(duration)
                .icon(transparent)
                .anchor(0.5F, 0.5F);

        mMap.addMarker(options).showInfoWindow();

        for (LatLng latLng : PolyUtil.decode(encodedPoints)) {
            builder.include(latLng);
        }
        builder.build();
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 200), 1000, null);
    }

    public void getCarDirection(String URL, final TextView tvCar) {
        StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Directions result = new Gson().fromJson(response, Directions.class);
                    Directions.Route[] routes = result.getRoutes();

                    if (routes != null) {
                        Directions.Leg[] leg = routes[0].getLegs();
                        Step[] step = leg[0].getSteps();
                        tvCar.setText(leg[0].getDuration().getText());
                        tvDistanceTime.setText(leg[0].getDuration().getText() + " (" + leg[0].getDistance().getText() + ")");
                        steps.clear();

                        for (int i = 0; i < step.length; i++) {
                            steps.add(step[i]);
                        }
                        adapter.notifyDataSetChanged();

                        addCarDirection(routes[0].getOverview_polyline().getPoints(), leg[0].getDuration().getText());
                    } else Toast.makeText(context, result.getStatus(), Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Lấy chỉ đường thất bại. Hãy thử lại.", Toast.LENGTH_SHORT).show();
                    }
                });
        VolleySingleton.getInstance(context).getRequestQueue().add(strReq);
    }

    public void getBusDirection(String URL, final TextView tvBus) {
        StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Directions result = new Gson().fromJson(response, Directions.class);
                    Directions.Route[] routes = result.getRoutes();

                    if (routes != null) {
                        Directions.Leg[] leg = routes[0].getLegs();
                        tvBus.setText(leg[0].getDuration().getText());
                        tvDistanceTime.setText(leg[0].getDuration().getText() + " (" + leg[0].getDistance().getText() + ")");
                        steps.clear();

                        Step[] step = leg[0].getSteps();
                        for (int j = 0; j < step.length; j++) {
                            steps.add(step[j]);
                        }

                        adapter.notifyDataSetChanged();
                        addBusDirection(routes);
                    } else Toast.makeText(context, result.getStatus(), Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Lấy chỉ đường thất bại. Hãy thử lại.", Toast.LENGTH_SHORT).show();
                    }
                });
        VolleySingleton.getInstance(context).getRequestQueue().add(strReq);
    }

    public void getWalkingDirection(String URL, final TextView tvWalking) {
        StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Directions result = new Gson().fromJson(response, Directions.class);
                    Directions.Route[] routes = result.getRoutes();

                    if (routes != null) {
                        Directions.Leg[] leg = routes[0].getLegs();
                        Step[] step = leg[0].getSteps();
                        tvWalking.setText(leg[0].getDuration().getText());
                        tvDistanceTime.setText(leg[0].getDuration().getText() + " (" + leg[0].getDistance().getText() + ")");
                        steps.clear();

                        for (int i = 0; i < step.length; i++) {
                            steps.add(step[i]);
                        }
                        adapter.notifyDataSetChanged();
                        addWalkingDirection(routes[0].getOverview_polyline().getPoints(), leg[0].getDuration().getText());
                    } else Toast.makeText(context, result.getStatus(), Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Lấy chỉ đường thất bại. Hãy thử lại.", Toast.LENGTH_SHORT).show();
                    }
                });
        VolleySingleton.getInstance(context).getRequestQueue().add(strReq);
    }

    public void getWalkingDirection1(String URL, final TextView tvWalking) {
        StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Directions result = new Gson().fromJson(response, Directions.class);
                    Directions.Route[] routes = result.getRoutes();

                    if (routes != null) {
                        Directions.Leg[] leg = routes[0].getLegs();
                        tvWalking.setText(leg[0].getDuration().getText());
                    } else Toast.makeText(context, result.getStatus(), Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Lấy chỉ đường thất bại. Hãy thử lại.", Toast.LENGTH_SHORT).show();
                    }
                });
        VolleySingleton.getInstance(context).getRequestQueue().add(strReq);
    }

    public void getBusDirection1(String URL, final TextView tvBus) {
        StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Directions result = new Gson().fromJson(response, Directions.class);
                    Directions.Route[] routes = result.getRoutes();

                    if (routes != null) {
                        Directions.Leg[] leg = routes[0].getLegs();
                        tvBus.setText(leg[0].getDuration().getText());
                    } else Toast.makeText(context, result.getStatus(), Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Lấy chỉ đường thất bại. Hãy thử lại.", Toast.LENGTH_SHORT).show();
                    }
                });
        VolleySingleton.getInstance(context).getRequestQueue().add(strReq);
    }
}
