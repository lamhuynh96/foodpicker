package ln2.foodpicker.adapter;


import android.content.Context;
import ln2.foodpicker.R;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.util.HashMap;

import ln2.foodpicker.model.FoodStall;

public class CustomInfowindowAdapter implements GoogleMap.InfoWindowAdapter {
    private Context context;
    private View view;
    private HashMap<String, FoodStall> markerInfo;

    public CustomInfowindowAdapter(Context context, HashMap<String, FoodStall> markerInfo) {
        this.context = context;
        this.markerInfo = markerInfo;
        view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.infowindow, null, false);
    }


    public View getInfoContents(Marker marker) {
        if (marker != null && marker.isInfoWindowShown()) {
            marker.hideInfoWindow();
            marker.showInfoWindow();
        }

        View v;
        if (markerInfo.get(marker.getId()) == null)
            v = null;
        else v = view;
        return v;
    }

    public View getInfoWindow(Marker marker) {
        ImageView thumbnail = (ImageView) view.findViewById(R.id.img_thumbnail);
        TextView tvName = (TextView) view.findViewById(R.id.tv_name);
        TextView tvAddress = (TextView) view.findViewById(R.id.tv_address);
        FoodStall stall = markerInfo.get(marker.getId());

        if (stall != null) {
            tvName.setText(stall.getStallName());
            tvAddress.setText(stall.getAddress() + ", " + stall.getDistrict());

            if (!stall.getImage_path().isEmpty()) {
                Glide.with(context).load(stall.getImage_path()).into(thumbnail);
            }
        }
        return null;
    }
}

