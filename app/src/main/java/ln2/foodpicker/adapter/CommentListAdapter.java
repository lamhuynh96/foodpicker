package ln2.foodpicker.adapter;

import android.content.Context;
import ln2.foodpicker.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ln2.foodpicker.activity.FoodActivity;
import ln2.foodpicker.model.Comment;

import static com.bumptech.glide.load.engine.DiskCacheStrategy.NONE;

public class CommentListAdapter extends ArrayAdapter<Comment> {
    private Context context;
    private ArrayList<Comment> comments;

    public CommentListAdapter(Context context, ArrayList<Comment> comments) {
        super(context, 0, comments);
        this.context = context;
        this.comments = comments;
    }

    private static class ViewHolder {
        TextView tvName;
        TextView tvComment;
        TextView tvRating;
        ImageView imageUser;
    }

    ViewHolder viewHolder;

    @Override
    public int getCount() {
        return comments.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Comment comment = comments.get(position);

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.comment_item, parent, false);
            viewHolder.tvName = (TextView) convertView.findViewById(R.id.tv_name);
            viewHolder.tvRating = (TextView) convertView.findViewById(R.id.tv_rating);
            viewHolder.tvComment = (TextView) convertView.findViewById(R.id.tv_comment);
            viewHolder.imageUser = (ImageView) convertView.findViewById(R.id.img_user);
            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.tvName.setText(comment.getUserName());
        viewHolder.tvRating.setText(comment.getRating());
        viewHolder.tvComment.setText(comment.getCmt());

        if (comment.getImage_path() != null) {
            Glide.with(context)
                    .load(comment.getImage_path())
                    .crossFade()
                    .thumbnail(0.5f)
                    .diskCacheStrategy(NONE)
                    .skipMemoryCache(true)
                    .bitmapTransform(new CircleTransform(context))
                    .into(viewHolder.imageUser);
        }
        return convertView;
    }
}
