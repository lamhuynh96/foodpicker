package ln2.foodpicker.adapter;

import android.content.Context;
import ln2.foodpicker.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import ln2.foodpicker.model.FoodStall;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class FavoriteListAdapter extends ArrayAdapter<FoodStall> {
    private Context context;
    private ArrayList<FoodStall> stalls;
    private TextView tvName;
    private TextView tvAddress;
    private ImageView image;
    private RelativeLayout layoutSelected;
    private LinearLayout layoutFavorite;

    public FavoriteListAdapter(Context context, ArrayList<FoodStall> stalls) {
        super(context, 0, stalls);
        this.stalls = stalls;
        this.context = context;
    }

    @Override
    public int getCount() {
        return stalls.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final FoodStall stall = stalls.get(position);

            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.feed_item, parent, false);
            layoutFavorite = (LinearLayout) convertView.findViewById(R.id.feed_item);
            layoutSelected = (RelativeLayout) convertView.findViewById(R.id.layout_selected);
            tvName = (TextView) convertView.findViewById(R.id.tv_name);
            tvAddress = (TextView) convertView.findViewById(R.id.tv_address);
            image = (ImageView) convertView.findViewById(R.id.image);


        tvName.setText(stall.getStallName());
        tvAddress.setText(stall.getAddress() + ", " + stall.getDistrict());

        if (!stall.getImage_path().isEmpty()) {
            Glide.with(context).load(stall.getImage_path()).into(image);
        }
        return convertView;
    }
}
