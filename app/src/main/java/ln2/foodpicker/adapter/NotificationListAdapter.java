package ln2.foodpicker.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ln2.foodpicker.R;
import ln2.foodpicker.activity.DetailActivity;
import ln2.foodpicker.model.Notification;
import ln2.foodpicker.util.VolleySingleton;

public class NotificationListAdapter extends ArrayAdapter<Notification> {
    private Context context;
    private ArrayList<Notification> notifications;

    private static class ViewHolder {
        LinearLayout layoutNotification;
        TextView tvTitle;
        TextView tvMessage;
        TextView tvTime;
    }

    ViewHolder viewHolder;

    public NotificationListAdapter(Context context, ArrayList<Notification> notifications) {
        super(context, 0, notifications);
        this.context = context;
        this.notifications = notifications;
    }

    @Override
    public int getCount() {
        return notifications.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Notification notification = getItem(position);

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.notification_item, parent, false);
            viewHolder.layoutNotification = (LinearLayout) convertView.findViewById(R.id.layout_notification);
            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            viewHolder.tvMessage = (TextView) convertView.findViewById(R.id.tv_message);
            viewHolder.tvTime = (TextView) convertView.findViewById(R.id.tv_time);
            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();

        if (notification.getIsSeen() == 0) {
            viewHolder.layoutNotification.setBackgroundColor(Color.parseColor("#EDEDED"));
            viewHolder.tvTitle.setTextColor(Color.BLACK);
            viewHolder.tvTime.setTextColor(Color.BLACK);
            viewHolder.tvMessage.setTextColor(Color.BLACK);
        } else {
            viewHolder.layoutNotification.setBackgroundColor(Color.WHITE);
            viewHolder.tvTitle.setTextColor(Color.parseColor("#848484"));
            viewHolder.tvTime.setTextColor(Color.parseColor("#848484"));
            viewHolder.tvMessage.setTextColor(Color.parseColor("#848484"));
        }

        viewHolder.tvTitle.setText(notification.getTitle());
        viewHolder.tvMessage.setText(notification.getMessage());
        String date = convertDate(notification.getTime_notify());
        Log.i("date", date);

        if (date != null) {
            viewHolder.tvTime.setText(date);
        }

        viewHolder.layoutNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notification.setIsSeen(1);
                viewHolder.layoutNotification.setBackgroundColor(Color.WHITE);
                viewHolder.tvTitle.setTextColor(Color.parseColor("#848484"));
                viewHolder.tvTime.setTextColor(Color.parseColor("#848484"));
                viewHolder.tvMessage.setTextColor(Color.parseColor("#848484"));

                setNotificationSeen(context.getString(R.string.set_notification_seen) + notification.getNotifyId(), notification);
                Intent detail = new Intent(context, DetailActivity.class);
                detail.putExtra("detail", new Gson().toJson(notification.getStallInfo()));
                context.startActivity(detail);
            }
        });

        return convertView;
    }

    private String convertDate(String timeStamp) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(timeStamp);
            format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            return format.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void setNotificationSeen(String URL, final Notification notification) {
        StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        JSONObject res = new JSONObject(response);

                        if (res.getString("payload").equals("success")) {
                            notification.setIsSeen(1);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
        VolleySingleton.getInstance(context).getRequestQueue().add(strReq);
    }
}
