package ln2.foodpicker.adapter;

import android.content.Context;
import ln2.foodpicker.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import ln2.foodpicker.model.FoodStall;

public class FeedListAdapter extends ArrayAdapter<FoodStall> {
    private Context context;
    private ArrayList<FoodStall> stalls;

    private static class ViewHolder {
        TextView tvName;
        TextView tvAddress;
        ImageView image;
    }

    ViewHolder viewHolder;

    public FeedListAdapter(Context context, ArrayList<FoodStall> stalls) {
        super(context, 0, stalls);
        this.stalls = stalls;
        this.context = context;
    }

    @Override
    public int getCount() {
        return stalls.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final FoodStall stall = getItem(position);

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.feed_item, parent, false);
            viewHolder.tvName = (TextView) convertView.findViewById(R.id.tv_name);
            viewHolder.tvAddress = (TextView) convertView.findViewById(R.id.tv_address);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.image);
            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.tvName.setText(stall.getStallName());
        viewHolder.tvAddress.setText(stall.getAddress() + ", " + stall.getDistrict());

        if (stall.getImage_path() != null) {
            Glide.with(context).load(stall.getImage_path()).into(viewHolder.image);
        } else {
            Glide.with(context).load(R.drawable.logo).into(viewHolder.image);
        }

        return convertView;
    }
}
