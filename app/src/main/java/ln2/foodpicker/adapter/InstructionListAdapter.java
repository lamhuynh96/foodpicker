package ln2.foodpicker.adapter;


import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import ln2.foodpicker.R;
import ln2.foodpicker.model.Directions.Leg.Step;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class InstructionListAdapter extends ArrayAdapter<Step> {
    private Context context;
    private ArrayList<Step> steps;

    private static class ViewHolder {
        ImageView imgManeuver;
        TextView tvInstruction;
        TextView tvBus;
        TextView tvPassBy;
        TextView tvDistance;
        TextView tvDestination;
        LinearLayout layoutBus;
    }

    ViewHolder viewHolder;

    public InstructionListAdapter(Context context, ArrayList<Step> steps) {
        super(context, 0, steps);
        this.steps = steps;
        this.context = context;
    }

    @Override
    public int getCount() {
        return steps.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Step step = getItem(position);

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.instruction_item, parent, false);
            viewHolder.tvInstruction = (TextView) convertView.findViewById(R.id.tv_instruction);
            viewHolder.tvPassBy = (TextView) convertView.findViewById(R.id.tv_pass_by);
            viewHolder.tvBus = (TextView) convertView.findViewById(R.id.tv_bus);
            viewHolder.imgManeuver = (ImageView) convertView.findViewById(R.id.img_manuver);
            viewHolder.tvDistance = (TextView) convertView.findViewById(R.id.tv_distance);
            viewHolder.tvDestination = (TextView) convertView.findViewById(R.id.tv_destination);
            viewHolder.layoutBus = (LinearLayout) convertView.findViewById(R.id.layout_bus);
            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();

        if (step.getManeuver() != null) {
            switch(step.getManeuver()) {
                case "turn-left":
                    viewHolder.imgManeuver.setImageResource(R.drawable.ic_turn_left);
                    break;
                case "turn-right":
                    viewHolder.imgManeuver.setImageResource(R.drawable.ic_turn_right);
                    break;
                case "roundabout-right":
                    viewHolder.imgManeuver.setImageResource(R.drawable.ic_round_about_right);
                    break;
                case "roundabout-left":
                    viewHolder.imgManeuver.setImageResource(R.drawable.ic_round_about_left);
                    break;
                case "straight":
                    viewHolder.imgManeuver.setImageResource(R.drawable.ic_straight);
                    break;
            }
        } else  viewHolder.imgManeuver.setImageResource(R.drawable.ic_info);

        viewHolder.tvDestination.setVisibility(GONE);
        viewHolder.layoutBus.setVisibility(GONE);
        viewHolder.tvDistance.setText(step.getDistance().getText());

        if (step.getTravel_mode().equals("TRANSIT")) {
            viewHolder.tvDestination.setVisibility(VISIBLE);
            viewHolder.layoutBus.setVisibility(VISIBLE);
            viewHolder.tvInstruction.setText(step.getTransit_detail().getDeparture_stop().getName());
            viewHolder.tvBus.setText(step.getTransit_detail().getLine().getName());
            viewHolder.tvPassBy.setText(String.valueOf(step.getTransit_detail().getNum_stops()) + " trạm");
            viewHolder.tvDestination.setText(step.getTransit_detail().getArrival_stop().getName());
            return convertView;
        }

        if (step.getHtml_instructions().indexOf("div") != -1) {
            viewHolder.tvInstruction.setText(Html.fromHtml(step.getHtml_instructions().substring(0, (step.getHtml_instructions().indexOf("div") - 1))));
        } else {
            viewHolder.tvInstruction.setText(Html.fromHtml(step.getHtml_instructions()));
        }

        if (step.getHtml_instructions().indexOf("Băng") != -1) {
            viewHolder.tvPassBy.setText(Html.fromHtml(step.getHtml_instructions().substring(step.getHtml_instructions().indexOf("Băng"))));
        } else {
            viewHolder.tvPassBy.setVisibility(GONE);
        }

        return convertView;
    }
}
